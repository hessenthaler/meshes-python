
default:
	make clean; make distclean; make lin_to_quad-opt && make vessel-opt && make cube-opt && make distclean

lin_to_quad-default:
	python3 setup-lin_to_quad.py     build_ext --inplace && mv lin_to_quad*.so lib/lin_to_quad.so
lin_to_quad-opt:
	python3 setup-lin_to_quad-opt.py build_ext --inplace && mv lin_to_quad*.so lib/lin_to_quad.so

cube-default:
	python3 setup-cube.py build_ext --inplace && mv cube*.so lib/cubeC.so
cube-opt:
	python3 setup-cube-opt.py build_ext --inplace && mv cube*.so lib/cubeC.so

vessel-default:
	python3 setup-vessel.py build_ext --inplace && mv vesselC*.so lib/vesselC.so
vessel-opt:
	python3 setup-vessel-opt.py build_ext --inplace && mv vesselC*.so lib/vesselC.so
vessel-cython-test:
	cython3 -a misc/vesselC.pyx
vessel-cython-test-opt:
	cython3 -X boundscheck=False -X wraparound=False -X cdivision=False -a misc/vesselC.pyx
clean:
	rm *.so
distclean:
	rm -r build/
