# Python scripts to generate various types of meshes #

Files:

**line.py** - line elements discretizing 1D line (option to export with 2D coordinates)

**triangles.py** - triangles discretizing 2D rectangular domain

**quadrilateral.py** - quadrilaterals discretizing 2D rectangular domain

**cube.py** - hexahedra or tetrahedra discretizing 3D cuboid

**vessel.py** - hexahedra discretizing vessel wall, tetrahedra discretizing cylinder

**flow_around_cylinder_simple.py** - triangle elements discretizing 2D rectangular domain with hole.

Note: All mesh generators export linear and quadratic elements.

## Installation requirements

The mesh generators were developed on Linux. Packages required are:

- python2 **and** python3
- python2-dev **and** python3-dev
- python2-matplotlib **and** python3-matplotlib
- python2-cython **and** python3-cython
- python2-numpy **and** python3-numpy
- python2-scipy **and** python3-scipy
- python2-tk **and** python3-tk
