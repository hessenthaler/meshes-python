#!/bin/python
################################################################################
# MESH GENERATOR FOR CUBE DOMAIN
################################################################################
# export format: X/T files (CHeart)
################################################################################
#
# HEX element
#
#
# ### 8-node hexahedron, node numbers:
#
#                 4 - 3   8 - 7
#                 |   |   |   |
#                 2 - 1   6 - 5
#
# coordinates:   (1, 1, 0) ---- (0, 1, 0)     (1, 1, 1) ---- (0, 1, 1)
#                    |              |             |              |
#                (1, 0, 0) ---- (0, 0, 0)     (1, 0, 1) ---- (0, 0, 1)
#
#
# ### 27-node hexahedron, node numbers:
#
#                  bottom                       top
#
#                 4--13--3     22--21--20     8--27--7
#                 |      |      |      |      |      |
#                12  11  10    19  18  17    26  25  24
#                 |      |      |      |      |      |
#                 2---9--1     16--15--14     6--23--5
#
################################################################################
#
# TET element
#
#
# ### 8-node hexahedron, node numbers:
#
#                            y
#                           /
#                          /
#                         3
#                       /   \
#                     /      \          4
#                    /        \
#                  1----------2---> x
#
#
# ### 27-node hexahedron, node numbers:
#
#                            y
#                           /
#                          /
#                         3                       10
#                       /   \                    / \
#                     6      7                 /    \          4
#                    /        \              /       \
#                  1-----5-----2---> x      8_________9
#
#
################################################################################
#
# note: we first discretize the cube domain with hexahedra, then subdivide each
#       hexahedron into five tetrahedra if required by user
#
################################################################################
# author: Andreas Hessenthaler
#
# no redistribution, for personal use only
################################################################################

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys
from mpl_toolkits.mplot3d import Axes3D
sys.path.append("lib/")
import cubeC

def main():
    
    if (len(sys.argv) != 13):
        print("missing or invalid commandline arguments:")
        print("    python cube.py folder/filenameprefix lengthX lengthY lengthZ numX numY numZ originX originY originZ hexToTet datafolder/")
        return
    
    fileprefix = str(sys.argv[1])
    dataprefix = str(sys.argv[12])
    
    tol = 1.0e-6
    
    fnameXl = fileprefix+"_lin_FE.X"
    fnameTl = fileprefix+"_lin_FE.T"
    fnameBl = fileprefix+"_lin_FE.B"
    fnameXq = fileprefix+"_quad_FE.X"
    fnameTq = fileprefix+"_quad_FE.T"
    fnameBq = fileprefix+"_quad_FE.B"
    datal   = dataprefix+"SolidPres-0.D"
    dataq   = dataprefix+"Disp-0.D"
    solvelq = dataprefix+"SolVel-0.D"
    spaceq  = dataprefix+"SolidSpace-0.D"
    
    showl = False
    showq = False
    
    # domain extents
    lx = float(sys.argv[2])
    ly = float(sys.argv[3])
    lz = float(sys.argv[4])
    
    # number of elements
    nx = int(sys.argv[5])
    ny = int(sys.argv[6])
    nz = int(sys.argv[7])
    
    # min domain
    x0 = float(sys.argv[8])
    y0 = float(sys.argv[9])
    z0 = float(sys.argv[10])
    
    # convert hexahedral mesh to tet mesh?
    if (1 == int(sys.argv[11])):
        hexToTet = True
    else:
        hexToTet = False
    
    # spacing
    dx = lx / nx
    dy = ly / ny
    dz = lz / nz
    
    # patch IDs
    patch_x0 = 1
    patch_x1 = 2
    patch_y0 = 3
    patch_y1 = 4
    patch_z0 = 5
    patch_z1 = 6
    
    # sanity check to ensure inf-sup stability, post a warning after execution
    # note: an odd number of elements in each coordinate direction may cause
    # the inf-sup stability condition to be violated
    # if user applies Dirichlet BC on all tet surfaces
    postWarning = False
    if (hexToTet and (((nx % 2) == 1) or ((ny % 2) == 1) or ((nz % 2) == 1))):
            postWarning = True
    
    # vmax of tip
    vmax  = 3.0 * np.sin(0.5 * np.pi) * (0.59686 * np.pi / lx)**2.0 * np.sqrt(1.0e8 * ly**4.0 / 12.0 / 1.0)
    
    print(" ")
    print("compute meshgrid")
    Xl, Yl, Zl = np.meshgrid(np.arange(x0, x0+lx+tol, dx    ), np.arange(y0, y0+ly+tol, dy    ), np.arange(z0, z0+lz+tol, dz    ))
    xl = Xl.flatten()
    yl = Yl.flatten()
    zl = Zl.flatten()
    coordinatesl = np.column_stack((xl, yl, zl))
    numberOfElements = int(nx*ny*nz)
    numberOfNodesL   = int((nx+1)*(ny+1)*(nz+1))
    if not(hexToTet):
        numberOfNodesQ = int((2*nx+1)*(2*ny+1)*(2*nz+1))
        Xq, Yq, Zq = np.meshgrid(np.arange(x0, x0+lx+tol, dx*0.5), np.arange(y0, y0+ly+tol, dy*0.5), np.arange(z0, z0+lz+tol, dz*0.5))
        xq = Xq.flatten()
        yq = Yq.flatten()
        zq = Zq.flatten()
        coordinatesq = np.column_stack((xq, yq, zq))
    else:
        # if we convert hex to tet, then we don't use the center nodes
        numberOfNodesQ = int((2*nx+1)*(2*ny+1)*(2*nz+1)) - numberOfElements
        coordinatesq = np.zeros((numberOfNodesQ, 3), dtype=float)
        # copy lin nodes to beginning of quad nodes
        for i in range(0, numberOfNodesL, 1):
            coordinatesq[i][0:3:1] = coordinatesl[i][0:3:1]
    print(" number of elements : "+str(numberOfElements))
    print(" number of nodes -l : "+str(numberOfNodesL))
    print("                 -q : "+str(numberOfNodesQ))
    print(" ")
    print("set node numbers -l")
    nodeNumberL  = 0
    nodeNumbersL = 0*Xl
    pressureL    = 0*xl
    dispQ        = 0*coordinatesq
    solvelQ      = 0*coordinatesq
    for i in range(0, int(ny)+1, 1):
        for j in range(0, int(nx)+1, 1):
            for k in range(0, int(nz)+1, 1):
                nodeNumberL +=1
                nodeNumbersL[i][j][k] = nodeNumberL
    print("set node numbers -q")
    # either
#    for i in range(0, 2*int(ny)+1, 2):
#        for j in range(0, 2*int(nx)+1, 2):
#            for k in range(0, 2*int(nz)+1, 2):
#                nodeNumberQ +=1
#                nodeNumbersQ[i][j][k] = nodeNumberQ # all those nodes that coincide with lin-nodes, same node ID
#    if (nodeNumberQ != nodeNumberL):
#        print(">>>ERROR: invalid number of nodes")
#        return
#    for i in range(0, 2*int(ny)+1, 2):
#        for j in range(0, 2*int(nx)+1, 2):
#            for k in range(1, 2*int(nz), 2):
#                nodeNumberQ +=1
#                nodeNumbersQ[i][j][k] = nodeNumberQ # all those nodes in z-direction between lin-nodes
#    for i in range(0, 2*int(ny)+1, 2):
#        for j in range(1, 2*int(nx), 2):
#            for k in range(0, 2*int(nz)+1, 1):
#                nodeNumberQ +=1
#                nodeNumbersQ[i][j][k] = nodeNumberQ # all those missing nodes in x-direction between lin-nodes
#    for i in range(1, 2*int(ny), 2):
#        for j in range(0, 2*int(nx)+1, 1):
#            for k in range(0, 2*int(nz)+1, 1):
#                nodeNumberQ +=1
#                nodeNumbersQ[i][j][k] = nodeNumberQ # all those missing nodes in y-direction between lin-nodes
    # or
    if not(hexToTet):
        nodeNumberQ = 0
        nodeNumbersQ = 0*Xq
        for i in range(0, 2*int(ny)+1, 1):
            for j in range(0, 2*int(nx)+1, 1):
                for k in range(0, 2*int(nz)+1, 1):
                    nodeNumberQ +=1
                    nodeNumbersQ[i][j][k] = nodeNumberQ
        if (nodeNumberQ != numberOfNodesQ):
            print(">>>ERROR: invalid number of nodes: "+str(nodeNumberQ))
            return
    else:
        # compute it later
        1.0
    # end
    print("set element node numbers -l")
    if (hexToTet):
        numberOfPatches = 2 * int(nx * ny + ny * nz + nx * nz) * 2
        patchesL        = np.zeros((numberOfPatches,  5), dtype=int)
        patchesQ        = np.zeros((numberOfPatches,  8), dtype=int)
    else:
        numberOfPatches = 2 * int(nx * ny + ny * nz + nx * nz)
        patchesL        = np.zeros((numberOfPatches,  6), dtype=int)
        patchesQ        = np.zeros((numberOfPatches, 11), dtype=int)
    elementNumbersL = np.zeros((numberOfElements, 8), dtype=int)
    elementNumberL  = 0
    patch           = 0
    for i in range(0, int(ny), 1):
        for j in range(0, int(nx), 1):
            for k in range(0, int(nz), 1):
                elementNumbersL[elementNumberL][0] = nodeNumbersL[i  ][j  ][k  ]
                elementNumbersL[elementNumberL][1] = nodeNumbersL[i  ][j  ][k+1]
                elementNumbersL[elementNumberL][2] = nodeNumbersL[i  ][j+1][k  ]
                elementNumbersL[elementNumberL][3] = nodeNumbersL[i  ][j+1][k+1]
                elementNumbersL[elementNumberL][4] = nodeNumbersL[i+1][j  ][k  ]
                elementNumbersL[elementNumberL][5] = nodeNumbersL[i+1][j  ][k+1]
                elementNumbersL[elementNumberL][6] = nodeNumbersL[i+1][j+1][k  ]
                elementNumbersL[elementNumberL][7] = nodeNumbersL[i+1][j+1][k+1]
                elementNumberL += 1
                # finding patches not required for hex if using tets anyway
                if not(hexToTet):
                    # patch at y = y0
                    if (i == 0):
                        patchesL[patch][0] = elementNumberL
                        patchesL[patch][1] = nodeNumbersL[i  ][j  ][k  ]
                        patchesL[patch][2] = nodeNumbersL[i  ][j  ][k+1]
                        patchesL[patch][3] = nodeNumbersL[i  ][j+1][k  ]
                        patchesL[patch][4] = nodeNumbersL[i  ][j+1][k+1]
                        patchesL[patch][5] = patch_y0
                        patch += 1
                    # patch at y = y1
                    if (i == int(ny-1)):
                        patchesL[patch][0] = elementNumberL
                        patchesL[patch][1] = nodeNumbersL[i+1][j  ][k  ]
                        patchesL[patch][2] = nodeNumbersL[i+1][j  ][k+1]
                        patchesL[patch][3] = nodeNumbersL[i+1][j+1][k  ]
                        patchesL[patch][4] = nodeNumbersL[i+1][j+1][k+1]
                        patchesL[patch][5] = patch_y1
                        patch += 1
                    # patch at x = x0
                    if (j == 0):
                        patchesL[patch][0] = elementNumberL
                        patchesL[patch][1] = nodeNumbersL[i  ][j  ][k  ]
                        patchesL[patch][2] = nodeNumbersL[i  ][j  ][k+1]
                        patchesL[patch][3] = nodeNumbersL[i+1][j  ][k  ]
                        patchesL[patch][4] = nodeNumbersL[i+1][j  ][k+1]
                        patchesL[patch][5] = patch_x0
                        patch += 1
                    # patch at x = x1
                    if (j == int(nx-1)):
                        patchesL[patch][0] = elementNumberL
                        patchesL[patch][1] = nodeNumbersL[i  ][j+1][k  ]
                        patchesL[patch][2] = nodeNumbersL[i  ][j+1][k+1]
                        patchesL[patch][3] = nodeNumbersL[i+1][j+1][k  ]
                        patchesL[patch][4] = nodeNumbersL[i+1][j+1][k+1]
                        patchesL[patch][5] = patch_x1
                        patch += 1
                    # patch at z = z0
                    if (k == 0):
                        patchesL[patch][0] = elementNumberL
                        patchesL[patch][1] = nodeNumbersL[i  ][j  ][k  ]
                        patchesL[patch][2] = nodeNumbersL[i  ][j+1][k  ]
                        patchesL[patch][3] = nodeNumbersL[i+1][j  ][k  ]
                        patchesL[patch][4] = nodeNumbersL[i+1][j+1][k  ]
                        patchesL[patch][5] = patch_z0
                        patch += 1
                    # patch at z = z1
                    if (k == int(nz-1)):
                        patchesL[patch][0] = elementNumberL
                        patchesL[patch][1] = nodeNumbersL[i  ][j  ][k+1]
                        patchesL[patch][2] = nodeNumbersL[i  ][j+1][k+1]
                        patchesL[patch][3] = nodeNumbersL[i+1][j  ][k+1]
                        patchesL[patch][4] = nodeNumbersL[i+1][j+1][k+1]
                        patchesL[patch][5] = patch_z1
                        patch += 1
    if ((patch != numberOfPatches) and not(hexToTet)):
        print(">>>ERROR: invalid number of patches -l: "+str(patch)+" "+str(numberOfPatches))
        return
    print("set element node numbers -q")
    if not(hexToTet):
        elementNumbersQ = np.zeros((numberOfElements, 27), dtype=int)
        elementNumberQ  = 0
        patch           = 0
        for i in range(0, int(ny), 1):
            for j in range(0, int(nx), 1):
                for k in range(0, int(nz), 1):
                    elementNumbersQ[elementNumberQ][ 0] = nodeNumbersQ[2*i  ][2*j  ][2*k  ]
                    elementNumbersQ[elementNumberQ][ 1] = nodeNumbersQ[2*i  ][2*j  ][2*k+2]
                    elementNumbersQ[elementNumberQ][ 2] = nodeNumbersQ[2*i  ][2*j+2][2*k  ]
                    elementNumbersQ[elementNumberQ][ 3] = nodeNumbersQ[2*i  ][2*j+2][2*k+2]
                    elementNumbersQ[elementNumberQ][ 4] = nodeNumbersQ[2*i+2][2*j  ][2*k  ]
                    elementNumbersQ[elementNumberQ][ 5] = nodeNumbersQ[2*i+2][2*j  ][2*k+2]
                    elementNumbersQ[elementNumberQ][ 6] = nodeNumbersQ[2*i+2][2*j+2][2*k  ]
                    elementNumbersQ[elementNumberQ][ 7] = nodeNumbersQ[2*i+2][2*j+2][2*k+2]
                    elementNumbersQ[elementNumberQ][ 8] = nodeNumbersQ[2*i  ][2*j  ][2*k+1]
                    elementNumbersQ[elementNumberQ][ 9] = nodeNumbersQ[2*i  ][2*j+1][2*k  ]
                    elementNumbersQ[elementNumberQ][10] = nodeNumbersQ[2*i  ][2*j+1][2*k+1]
                    elementNumbersQ[elementNumberQ][11] = nodeNumbersQ[2*i  ][2*j+1][2*k+2]
                    elementNumbersQ[elementNumberQ][12] = nodeNumbersQ[2*i  ][2*j+2][2*k+1]
                    elementNumbersQ[elementNumberQ][13] = nodeNumbersQ[2*i+1][2*j  ][2*k  ]
                    elementNumbersQ[elementNumberQ][14] = nodeNumbersQ[2*i+1][2*j  ][2*k+1]
                    elementNumbersQ[elementNumberQ][15] = nodeNumbersQ[2*i+1][2*j  ][2*k+2]
                    elementNumbersQ[elementNumberQ][16] = nodeNumbersQ[2*i+1][2*j+1][2*k  ]
                    elementNumbersQ[elementNumberQ][17] = nodeNumbersQ[2*i+1][2*j+1][2*k+1]
                    elementNumbersQ[elementNumberQ][18] = nodeNumbersQ[2*i+1][2*j+1][2*k+2]
                    elementNumbersQ[elementNumberQ][19] = nodeNumbersQ[2*i+1][2*j+2][2*k  ]
                    elementNumbersQ[elementNumberQ][20] = nodeNumbersQ[2*i+1][2*j+2][2*k+1]
                    elementNumbersQ[elementNumberQ][21] = nodeNumbersQ[2*i+1][2*j+2][2*k+2]
                    elementNumbersQ[elementNumberQ][22] = nodeNumbersQ[2*i+2][2*j  ][2*k+1]
                    elementNumbersQ[elementNumberQ][23] = nodeNumbersQ[2*i+2][2*j+1][2*k  ]
                    elementNumbersQ[elementNumberQ][24] = nodeNumbersQ[2*i+2][2*j+1][2*k+1]
                    elementNumbersQ[elementNumberQ][25] = nodeNumbersQ[2*i+2][2*j+1][2*k+2]
                    elementNumbersQ[elementNumberQ][26] = nodeNumbersQ[2*i+2][2*j+2][2*k+1]
                    elementNumberQ += 1
                    # finding patches not required for hex if using tets anyway
                    if not(hexToTet):
                        # patch at y = y0
                        if (i == 0):
                            patchesQ[patch][ 0] = elementNumberQ
                            patchesQ[patch][ 1] = nodeNumbersQ[2*i  ][2*j  ][2*k  ]
                            patchesQ[patch][ 2] = nodeNumbersQ[2*i  ][2*j  ][2*k+2]
                            patchesQ[patch][ 3] = nodeNumbersQ[2*i  ][2*j+2][2*k  ]
                            patchesQ[patch][ 4] = nodeNumbersQ[2*i  ][2*j+2][2*k+2]
                            patchesQ[patch][ 5] = nodeNumbersQ[2*i  ][2*j  ][2*k+1]
                            patchesQ[patch][ 6] = nodeNumbersQ[2*i  ][2*j+1][2*k  ]
                            patchesQ[patch][ 7] = nodeNumbersQ[2*i  ][2*j+1][2*k+1]
                            patchesQ[patch][ 8] = nodeNumbersQ[2*i  ][2*j+1][2*k+2]
                            patchesQ[patch][ 9] = nodeNumbersQ[2*i  ][2*j+2][2*k+1]
                            patchesQ[patch][10] = patch_y0
                            patch += 1
                        # patch at y = y1
                        if (i == int(ny-1)):
                            patchesQ[patch][ 0] = elementNumberQ
                            patchesQ[patch][ 1] = nodeNumbersQ[2*i+2][2*j  ][2*k  ]
                            patchesQ[patch][ 2] = nodeNumbersQ[2*i+2][2*j  ][2*k+2]
                            patchesQ[patch][ 3] = nodeNumbersQ[2*i+2][2*j+2][2*k  ]
                            patchesQ[patch][ 4] = nodeNumbersQ[2*i+2][2*j+2][2*k+2]
                            patchesQ[patch][ 5] = nodeNumbersQ[2*i+2][2*j  ][2*k+1]
                            patchesQ[patch][ 6] = nodeNumbersQ[2*i+2][2*j+1][2*k  ]
                            patchesQ[patch][ 7] = nodeNumbersQ[2*i+2][2*j+1][2*k+1]
                            patchesQ[patch][ 8] = nodeNumbersQ[2*i+2][2*j+1][2*k+2]
                            patchesQ[patch][ 9] = nodeNumbersQ[2*i+2][2*j+2][2*k+1]
                            patchesQ[patch][10] = patch_y1
                            patch += 1
                        # patch at x = x0
                        if (j == 0):
                            patchesQ[patch][ 0] = elementNumberQ
                            patchesQ[patch][ 1] = nodeNumbersQ[2*i  ][2*j  ][2*k  ]
                            patchesQ[patch][ 2] = nodeNumbersQ[2*i  ][2*j  ][2*k+2]
                            patchesQ[patch][ 3] = nodeNumbersQ[2*i+2][2*j  ][2*k  ]
                            patchesQ[patch][ 4] = nodeNumbersQ[2*i+2][2*j  ][2*k+2]
                            patchesQ[patch][ 5] = nodeNumbersQ[2*i  ][2*j  ][2*k+1]
                            patchesQ[patch][ 6] = nodeNumbersQ[2*i+1][2*j  ][2*k  ]
                            patchesQ[patch][ 7] = nodeNumbersQ[2*i+1][2*j  ][2*k+1]
                            patchesQ[patch][ 8] = nodeNumbersQ[2*i+1][2*j  ][2*k+2]
                            patchesQ[patch][ 9] = nodeNumbersQ[2*i+2][2*j  ][2*k+1]
                            patchesQ[patch][10] = patch_x0
                            patch += 1
                        # patch at x = x1
                        if (j == int(nx-1)):
                            patchesQ[patch][ 0] = elementNumberQ
                            patchesQ[patch][ 1] = nodeNumbersQ[2*i  ][2*j+2][2*k  ]
                            patchesQ[patch][ 2] = nodeNumbersQ[2*i  ][2*j+2][2*k+2]
                            patchesQ[patch][ 3] = nodeNumbersQ[2*i+2][2*j+2][2*k  ]
                            patchesQ[patch][ 4] = nodeNumbersQ[2*i+2][2*j+2][2*k+2]
                            patchesQ[patch][ 5] = nodeNumbersQ[2*i  ][2*j+2][2*k+1]
                            patchesQ[patch][ 6] = nodeNumbersQ[2*i+1][2*j+2][2*k  ]
                            patchesQ[patch][ 7] = nodeNumbersQ[2*i+1][2*j+2][2*k+1]
                            patchesQ[patch][ 8] = nodeNumbersQ[2*i+1][2*j+2][2*k+2]
                            patchesQ[patch][ 9] = nodeNumbersQ[2*i+2][2*j+2][2*k+1]
                            patchesQ[patch][10] = patch_x1
                            patch += 1
                        # patch at z = z0
                        if (k == 0):
                            patchesQ[patch][ 0] = elementNumberQ
                            patchesQ[patch][ 1] = nodeNumbersQ[2*i  ][2*j  ][2*k  ]
                            patchesQ[patch][ 2] = nodeNumbersQ[2*i  ][2*j+2][2*k  ]
                            patchesQ[patch][ 3] = nodeNumbersQ[2*i+2][2*j  ][2*k  ]
                            patchesQ[patch][ 4] = nodeNumbersQ[2*i+2][2*j+2][2*k  ]
                            patchesQ[patch][ 5] = nodeNumbersQ[2*i  ][2*j+1][2*k  ]
                            patchesQ[patch][ 6] = nodeNumbersQ[2*i+1][2*j  ][2*k  ]
                            patchesQ[patch][ 7] = nodeNumbersQ[2*i+1][2*j+1][2*k  ]
                            patchesQ[patch][ 8] = nodeNumbersQ[2*i+1][2*j+2][2*k  ]
                            patchesQ[patch][ 9] = nodeNumbersQ[2*i+2][2*j+1][2*k  ]
                            patchesQ[patch][10] = patch_z0
                            patch += 1
                        # patch at z = z0
                        if (k == int(nz-1)):
                            patchesQ[patch][ 0] = elementNumberQ
                            patchesQ[patch][ 1] = nodeNumbersQ[2*i  ][2*j  ][2*k+2]
                            patchesQ[patch][ 2] = nodeNumbersQ[2*i  ][2*j+2][2*k+2]
                            patchesQ[patch][ 3] = nodeNumbersQ[2*i+2][2*j  ][2*k+2]
                            patchesQ[patch][ 4] = nodeNumbersQ[2*i+2][2*j+2][2*k+2]
                            patchesQ[patch][ 5] = nodeNumbersQ[2*i  ][2*j+1][2*k+2]
                            patchesQ[patch][ 6] = nodeNumbersQ[2*i+1][2*j  ][2*k+2]
                            patchesQ[patch][ 7] = nodeNumbersQ[2*i+1][2*j+1][2*k+2]
                            patchesQ[patch][ 8] = nodeNumbersQ[2*i+1][2*j+2][2*k+2]
                            patchesQ[patch][ 9] = nodeNumbersQ[2*i+2][2*j+1][2*k+2]
                            patchesQ[patch][10] = patch_z1
                            patch += 1
        if (patch != numberOfPatches):
            print(">>>ERROR: invalid number of patches -l: "+str(patch)+" "+str(numberOfPatches))
            return
    # convert hex to tet mesh
    else:
        print(" -converting to tet mesh")
        # we split each hexahedron into five tetrahedra
        numberOfElementsTet = numberOfElements * 5
        elementNumbersLtet  = np.zeros((numberOfElementsTet,  4), dtype=int)
        elementNumbersQtet  = np.zeros((numberOfElementsTet, 10), dtype=int)
        elementNumberLtet   = 0
        # note: we need to rotate every other hexahedron to ensure
        #       conforming inner faces when splitting into tetrahedra;
        #       to do this, we create a 3D checkerboard pattern to decide
        #       whether or not to rotate the hexahedron considered
        checkerBoard3D      = np.zeros((numberOfElementsTet, 1), dtype=int)
        hexIdx              = 0
        for i in range(0, ny, 1):
            for j in range(0, nx, 1):
                for k in range(0, nz, 1):
                    if (((k+((j+(i % 2)) % 2)) % 2) == 1):
                        checkerBoard3D[hexIdx] = 1
                    hexIdx += 1
        # loop over all hexahedra in the mesh
        for i in range(0, numberOfElements, 1):
            # rotate the hexahedron before splitting into tetrahedra?
            if (checkerBoard3D[i] == 1):
                # get element node IDs for rotated linear hexahedron
                n0 = elementNumbersL[i][1]
                n1 = elementNumbersL[i][3]
                n2 = elementNumbersL[i][0]
                n3 = elementNumbersL[i][2]
                n4 = elementNumbersL[i][5]
                n5 = elementNumbersL[i][7]
                n6 = elementNumbersL[i][4]
                n7 = elementNumbersL[i][6]
            else:
                # get element node IDs for linear hexahedron
                n0 = elementNumbersL[i][0]
                n1 = elementNumbersL[i][1]
                n2 = elementNumbersL[i][2]
                n3 = elementNumbersL[i][3]
                n4 = elementNumbersL[i][4]
                n5 = elementNumbersL[i][5]
                n6 = elementNumbersL[i][6]
                n7 = elementNumbersL[i][7]
            # assign element node IDs for linear tetrahedra - element 1
            elementNumbersLtet[elementNumberLtet][0] = n0
            elementNumbersLtet[elementNumberLtet][1] = n1
            elementNumbersLtet[elementNumberLtet][2] = n3
            elementNumbersLtet[elementNumberLtet][3] = n5
            elementNumberLtet += 1
            # assign element node IDs for linear tetrahedra - element 2
            elementNumbersLtet[elementNumberLtet][0] = n0
            elementNumbersLtet[elementNumberLtet][1] = n5
            elementNumbersLtet[elementNumberLtet][2] = n3
            elementNumbersLtet[elementNumberLtet][3] = n6
            elementNumberLtet += 1
            # assign element node IDs for linear tetrahedra - element 3
            elementNumbersLtet[elementNumberLtet][0] = n5
            elementNumbersLtet[elementNumberLtet][1] = n3
            elementNumbersLtet[elementNumberLtet][2] = n6
            elementNumbersLtet[elementNumberLtet][3] = n7
            elementNumberLtet += 1
            # assign element node IDs for linear tetrahedra - element 4
            elementNumbersLtet[elementNumberLtet][0] = n0
            elementNumbersLtet[elementNumberLtet][1] = n5
            elementNumbersLtet[elementNumberLtet][2] = n6
            elementNumbersLtet[elementNumberLtet][3] = n4
            elementNumberLtet += 1
            # assign element node IDs for linear tetrahedra - element 5
            elementNumbersLtet[elementNumberLtet][0] = n0
            elementNumbersLtet[elementNumberLtet][1] = n3
            elementNumbersLtet[elementNumberLtet][2] = n2
            elementNumbersLtet[elementNumberLtet][3] = n6
            elementNumberLtet += 1
        if (elementNumberLtet != numberOfElementsTet):
            print(">>>ERROR: invalid number of linear tets"+str(elementNumberLtet)+" "+str(numberOfElementsTet))
            return
        # create quad topology using library
        pointsL = xl, yl, zl
        pointsL = np.zeros((numberOfNodesL*3), dtype=float)
        pointsL[0*numberOfNodesL:1*numberOfNodesL:1] = xl
        pointsL[1*numberOfNodesL:2*numberOfNodesL:1] = yl
        pointsL[2*numberOfNodesL:3*numberOfNodesL:1] = zl
        numberOfNodesQ, maxNumberOfNodesQ, pointsQ, patchesL, patchesQ \
            = cubeC.lin2quad(pointsL, elementNumbersLtet, elementNumbersQtet, \
                numberOfNodesL, numberOfElementsTet, numberOfPatches, \
                x0, x0+lx, y0, y0+ly, z0, z0+lz, \
                patch_x0, patch_x1, patch_y0, patch_y1, patch_z0, patch_z1, tol)
        coordinatesq = np.delete( \
            pointsQ.reshape((3, maxNumberOfNodesQ)).transpose(), \
            range(numberOfNodesQ, maxNumberOfNodesQ, 1), \
            axis=0)
        xq = pointsQ[0*maxNumberOfNodesQ:0*maxNumberOfNodesQ+numberOfNodesQ:1]
        yq = pointsQ[1*maxNumberOfNodesQ:1*maxNumberOfNodesQ+numberOfNodesQ:1]
        zq = pointsQ[2*maxNumberOfNodesQ:2*maxNumberOfNodesQ+numberOfNodesQ:1]
        if (cubeC.sanityCheckUniqueNodes(xl, yl, zl, xq, yq, zq, tol) == -1):
            return
        # overwrite element topology (hex --> tet)
        elementNumbersL     = elementNumbersLtet
        elementNumbersQ     = elementNumbersQtet
        numberOfElements    = numberOfElementsTet
    if hexToTet:
        for i in range(0, numberOfPatches, 1):
            for j in range(1, 4, 1):
                pressureL[patchesL[i][j]-1] = patchesL[i][4]
    else:
        for i in range(0, numberOfPatches, 1):
            for j in range(1, 5, 1):
                if ((pressureL[patchesL[i][j]-1] != patch_x0) \
                    and (pressureL[patchesL[i][j]-1] != patch_x1)):
                    pressureL[patchesL[i][j]-1] = patchesL[i][5]
    if hexToTet:
        for i in range(0, numberOfPatches, 1):
            for j in range(1, 7, 1):
                solvelQ[patchesQ[i][j]-1][2] = patchesQ[i][7]
    else:
        for i in range(0, numberOfPatches, 1):
            for j in range(1, 10, 1):
                if ((dispQ[patchesQ[i][j]-1][0] != patch_x0) \
                    and (dispQ[patchesQ[i][j]-1][0] != patch_x1)):
                    dispQ[patchesQ[i][j]-1][0] = patchesQ[i][10]
        for i in range(0, numberOfNodesQ, 1):
            solvelQ[i][2] = vmax * coordinatesq[i][0] / lx
    
    print(" ")
    
    print("export "+datal)
    myheader = " "+str(numberOfNodesL)+" 1"
    np.savetxt(datal, pressureL, fmt="%f", header=myheader, comments="")
    print("export "+dataq)
    myheader = " "+str(numberOfNodesQ)+" 3"
    np.savetxt(dataq, dispQ, fmt="%f", header=myheader, comments="")
    print("export "+spaceq)
    myheader = " "+str(numberOfNodesQ)+" 3"
    np.savetxt(spaceq, coordinatesq, fmt="%f", header=myheader, comments="")
    print("export "+solvelq)
    myheader = " "+str(numberOfNodesQ)+" 3"
    np.savetxt(solvelq, solvelQ, fmt="%f", header=myheader, comments="")
    
    print("export X -l")
    myheader = " "+str(numberOfNodesL)+" 3"
    np.savetxt(fnameXl, coordinatesl, fmt="%f", header=myheader, comments="")
    print("export X -q")
    myheader = " "+str(numberOfNodesQ)+" 3"
    np.savetxt(fnameXq, coordinatesq, fmt="%f", header=myheader, comments="")
    
    print("export T -l")
    myheader = " "+str(numberOfElements)+" "+str(numberOfNodesL)
    np.savetxt(fnameTl, elementNumbersL, fmt="%d", header=myheader, comments="")
    print("export T -q")
    myheader = " "+str(numberOfElements)+" "+str(numberOfNodesQ)
    np.savetxt(fnameTq, elementNumbersQ, fmt="%d", header=myheader, comments="")
    
    print("export B -l")
    myheader = " "+str(numberOfPatches)
    np.savetxt(fnameBl, patchesL, fmt="%d", header=myheader, comments="")
    print("export B -q")
    myheader = " "+str(numberOfPatches)
    np.savetxt(fnameBq, patchesQ, fmt="%d", header=myheader, comments="")
    
    print(" ")
    
    if showl:
        print("show mesh -l")
        mycolors = pressureL
        colormap = plt.get_cmap('coolwarm')
        colorsNormalized = matplotlib.colors.Normalize(vmin=min(mycolors), vmax=max(mycolors))
        scalarMap = matplotlib.cm.ScalarMappable(norm=colorsNormalized, cmap=colormap)
        fig = plt.figure()
        ax = Axes3D(fig)
        axscatter = ax.scatter(xl, yl, zl, c=scalarMap.to_rgba(mycolors))
        scalarMap.set_array(mycolors)
        fig.colorbar(scalarMap)
        plt.show()
    
    if showq:
        print("show mesh -q")
        mycolors = dispQ[:, 0]
        colormap = plt.get_cmap('coolwarm')
        colorsNormalized = matplotlib.colors.Normalize(vmin=min(mycolors), vmax=max(mycolors))
        scalarMap = matplotlib.cm.ScalarMappable(norm=colorsNormalized, cmap=colormap)
        fig = plt.figure()
        ax = Axes3D(fig)
        axscatter = ax.scatter(xq, yq, zq, c=scalarMap.to_rgba(mycolors))
        scalarMap.set_array(mycolors)
        fig.colorbar(scalarMap)
        plt.show()
    if postWarning:
        print(">>>WARNING: inf-sup stability may be violated on corner-tets" \
            + " if you apply Dirichlet BC on all quad-surfaces. Careful!")
        print(">>>WARNING: use an even number of elements in each coordinate direction for safety!")
        print(" ")

if __name__ == "__main__":
    main()
