#!/bin/python
################################################################################
# MESH GENERATOR FOR FLOW AROUND CYLINDER
################################################################################
# export format: X/T/B files (CHeart)
################################################################################
#
# geometry:     --------------------------      -
#               |                        |      |
#               |     --                 |      |
#               |    |  |                |      | ly
#               |     --                 |      |
#               |                        |      |
#               +-------------------------      -
#          origin
#
#
#               <---------- lx ---------->
#
#
#
# coordinate system:      y
#                         |
#                         |___ x
#
################################################################################
#
# testing examples for FSIViewer:
#
#   python3 flow_around_cylinder_simple.py meshes/domainF 40 220 41 data/ 1 1
#
################################################################################
#
# TRI mesh for fluid domain
#
### 3-node/6-node triangle, node numbers:
#
#                            y
#                           /
#                          /
#                         3
#                       /   \
#                     5      6
#                    /        \
#                  1-----4-----2---> x
#
################################################################################
#
# Required packages:    python3, python3-dev    >= 3.4
#                       numpy                   >= 1.11.2
#                       scipy                   >= 0.13
#                       cython3                 >= 0.23.4
#
################################################################################
# author: Andreas Hessenthaler
#
# no redistribution, for personal use only
################################################################################
import numpy as np
import sys
#import matplotlib
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay, delaunay_plot_2d
from scipy import version as scipy_version
sys.path.append("lib/")
import lin_to_quad
################################################################################
# check version
################################################################################
scipyVersion = scipy_version.version
scipyVersionMajor = int(scipyVersion[0])
scipyVersionMinor = int(scipyVersion[2:4:1])
# for oriented Delaunay triangulation in 2D
if ((scipyVersionMajor == 0) and (scipyVersionMinor <= 13)):
    print(">>>ERROR: Requires scipy version newer than 0.13. You have " \
        +str(scipyVersionMajor)+"."+str(scipyVersionMinor)+"!")
    sys.exit()
################################################################################
# get commandline arguments
################################################################################
# check whether there are enough arguments
if (len(sys.argv) != 10):
    print("missing or invalid commandline arguments:")
    print("")
    print("    python flow_around_cylinder_simple.py param1 param2 param3 param4 param5 param6 param7 param8")
    print("")
    print("                     param1 - prefix for fluid (e.g. \'meshes/domainF\')")
    print("                     param2 - prefix for solid (e.g. \'meshes/domainS\')")
    print("                     param3 - prefix for interface (e.g. \'meshes/lm\')")
    print("                     param4 - number of subdivisions of cylinder surface")
    print("                     param5 - number of subdivisions in x-direction")
    print("                     param6 - number of subdivisions in y-direction")
    print("                     param7 - initial fields folder (e.g. \'data/\')")
    print("                     param8 - refine center of channel (0 - no, 1 - yes)")
    print("                     param9 - show lin/quad meshes (0 - no, 1 - yes)")
    print("")
    sys.exit()
# store arguments
fileprefixF =   str(sys.argv[1])
fileprefixS =   str(sys.argv[2])
fileprefixI =   str(sys.argv[3])
arg_nc      = float(sys.argv[4])
arg_nx      = float(sys.argv[5])
arg_ny      = float(sys.argv[6])
dataprefixF =   str(sys.argv[7])
refinecyl   = (1 == int(sys.argv[8]))
showmeshesF = (1 == int(sys.argv[9]))
################################################################################
# setting mesh sizes, etc.
################################################################################
# coordinate position of point at bottom left of domain
# if non-zero --> added after mesh generation
ox = 0.00
oy = 0.00
# length of domain in x-/y-direction
lx = 0.80 # 2.20
ly = 0.41
# position of cylinder
cx = 0.20
cy = 0.20
# diameter/radius of cylinder
cd = 0.10
cr = 0.50 * cd
# number of elements discretizing cylinder boundary
nc = arg_nc
# number of elements discretizing boundary at x=0, x=lx and y=oy, y=0
nx = arg_nx
ny = arg_ny
# patch labels
patchF_x0 = 1
patchF_x1 = 2
patchF_y0 = 3
patchF_y1 = 4
patchF_cy = 5
# set tolerances
cyltol = 0.5
cyledge = 2.0 * np.pi * cr / float(nc)
tol    = 1.0e-6
################################################################################
# fluid mesh generator
################################################################################

maxNumNodesLinF = (2 * nx + 1) * (2 * ny + 1) + nc
xlinF   = np.zeros((int(maxNumNodesLinF)), dtype=float)
ylinF   = np.zeros((int(maxNumNodesLinF)), dtype=float)
xlinS   = np.zeros((int(maxNumNodesLinF)), dtype=float)
ylinS   = np.zeros((int(maxNumNodesLinF)), dtype=float)
xlinI   = np.zeros((int(maxNumNodesLinF)), dtype=float)
ylinI   = np.zeros((int(maxNumNodesLinF)), dtype=float)
xquadI  = np.zeros((int(maxNumNodesLinF)), dtype=float)
yquadI  = np.zeros((int(maxNumNodesLinF)), dtype=float)

numNodesLinF    = 0
numNodesLinS    = 0
numNodesLinI    = 0
numNodesQuadI   = 0

# nodes on cylinder surface
xlinF[0:int(nc):1] = cx \
    + cr * np.cos(np.linspace(0.0, float(nc), nc, endpoint=False) * 2.0 \
    * np.pi / float(nc))
ylinF[0:int(nc):1] = cy \
    + cr * np.sin(np.linspace(0.0, float(nc), nc, endpoint=False) * 2.0 \
    * np.pi / float(nc))
numNodesLinF += int(nc)
xlinS[0:int(nc):1] = cx \
    + cr * np.cos(np.linspace(0.0, float(nc), nc, endpoint=False) * 2.0 \
    * np.pi / float(nc))
ylinS[0:int(nc):1] = cy \
    + cr * np.sin(np.linspace(0.0, float(nc), nc, endpoint=False) * 2.0 \
    * np.pi / float(nc))
numNodesLinS += int(nc)
xlinI[0:int(nc):1] = cx \
    + cr * np.cos(np.linspace(0.0, float(nc), nc, endpoint=False) * 2.0 \
    * np.pi / float(nc))
ylinI[0:int(nc):1] = cy \
    + cr * np.sin(np.linspace(0.0, float(nc), nc, endpoint=False) * 2.0 \
    * np.pi / float(nc))
numNodesLinI += int(nc)
xquadI[0:int(nc):1] = xlinI[0:int(nc):1]
yquadI[0:int(nc):1] = ylinI[0:int(nc):1]
numNodesQuadI      += int(nc)
for i in range(0, int(nc), 1):
    xquadI[int(nc)+i]   = 0.5 * (xquadI[i] + xquadI[i+1])
    yquadI[int(nc)+i]   = 0.5 * (yquadI[i] + yquadI[i+1])
    numNodesQuadI += 1

# regular grid
firstAdd = True
for j in range(0, int(ny+1), 1):
    for i in range(0, int(nx+1), 1):
        x = float(i) * lx / nx
        y = float(j) * ly / ny
        # only use nodes that are not inside cylinder
        if not(np.sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy)) <= cr+cyledge*cyltol):
            xlinF[numNodesLinF] = x
            ylinF[numNodesLinF] = y
            numNodesLinF += 1
        if not(np.sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy)) >= cr-cyledge*cyltol):
            xlinS[numNodesLinS] = x
            ylinS[numNodesLinS] = y
            numNodesLinS += 1
        if (refinecyl):
            if ((y > cy-cr-0.5*ly/ny) and (y < cy+cr+0.5*ly/ny)):
                if firstAdd:
                    firstAddJ = j
                    if (j != firstAddJ):
                        firstAdd = False
                    x = float(i) * lx / nx
                    y = float(j-0.5) * ly / ny
                    # only use nodes that are not inside cylinder
                    if not(np.sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy)) <= cr+cyledge*cyltol):
                        xlinF[numNodesLinF] = x
                        ylinF[numNodesLinF] = y
                        numNodesLinF += 1
            if ((y > cy+cr) and (y <= cy+cr+1*ly/ny)):
                x = float(i) * lx / nx
                y = float(j-0.5) * ly / ny
                # only use nodes that are not inside cylinder
                if not(np.sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy)) <= cr+cyledge*cyltol):
                    xlinF[numNodesLinF] = x
                    ylinF[numNodesLinF] = y
                    numNodesLinF += 1

# interface - lin / quad topo (1-based)
linesLinI   = np.zeros((int(nc), 2), dtype=float)
linesQuadI  = np.zeros((int(nc), 3), dtype=float)
for i in range(0, int(nc), 1):
    linesLinI[i][0]     = 1 + i
    linesLinI[i][1]     = 2 + i
    linesQuadI[i][0]    = 1 + i
    linesQuadI[i][1]    = 2 + i
    linesQuadI[i][2]    = 1 + nc + i
linesLinI[int(nc)-1][1]     = 1
linesQuadI[int(nc)-1][1]    = 1

# compute Delaunay triangulation
pointslinF  = np.column_stack((xlinF[0:numNodesLinF:1],     ylinF[0:numNodesLinF:1]))
pointslinS  = np.column_stack((xlinS[0:numNodesLinS:1],     ylinS[0:numNodesLinS:1]))
pointslinI  = np.column_stack((xlinI[0:numNodesLinI:1],     ylinI[0:numNodesLinI:1]))
pointsquadI = np.column_stack((xquadI[0:numNodesQuadI:1],   yquadI[0:numNodesQuadI:1]))
del xlinF, ylinF, xlinS, ylinS, xlinI, ylinI, xquadI, yquadI
xlinF  = np.delete(pointslinF,  range(1, 2, 1), axis=1).flatten()
ylinF  = np.delete(pointslinF,  range(0, 1, 1), axis=1).flatten()
xlinS  = np.delete(pointslinS,  range(1, 2, 1), axis=1).flatten()
ylinS  = np.delete(pointslinS,  range(0, 1, 1), axis=1).flatten()
trisF = Delaunay(pointslinF)
trisS = Delaunay(pointslinS)
numberOfTrianglesF = trisF.simplices.shape[0]
numberOfTrianglesS = trisS.simplices.shape[0]

# mask Delaunay triangulation (1 - valid, 0 - invalid)
numberOfInvalidTrianglesF = 0
maskTrianglesF = np.ones((numberOfTrianglesF), dtype=int)
for i in range(0, numberOfTrianglesF, 1):
    n0 = trisF.simplices[i][0]
    n1 = trisF.simplices[i][1]
    n2 = trisF.simplices[i][2]
    if ((n0 < nc) and (n1 < nc) and (n2 < nc)):
        maskTrianglesF[i] = 0
        numberOfInvalidTrianglesF += 1
numberOfValidTrianglesF = numberOfTrianglesF - numberOfInvalidTrianglesF
trilinF = np.zeros((numberOfValidTrianglesF, 3), dtype=int)
triiterF = 0
for i in range(0, numberOfTrianglesF, 1):
    if (maskTrianglesF[i] == 1):
        trilinF[triiterF][0] = trisF.simplices[i][0]
        trilinF[triiterF][1] = trisF.simplices[i][1]
        trilinF[triiterF][2] = trisF.simplices[i][2]
        triiterF += 1
trilinS = np.zeros((numberOfTrianglesS, 3), dtype=int)
triiterS = 0
for i in range(0, numberOfTrianglesS, 1):
    trilinS[triiterS][0] = trisS.simplices[i][0]
    trilinS[triiterS][1] = trisS.simplices[i][1]
    trilinS[triiterS][2] = trisS.simplices[i][2]
    triiterS += 1
# delete initial triangulation
del trisF, trisS
# also set this variable to counteract wrong use later on
numberOfTrianglesF = numberOfValidTrianglesF

# plot Delaunay triangulation
if showmeshesF:
    fig = plt.figure()
    plt.triplot(pointslinF[:,0], pointslinF[:,1], trilinF)
    plt.plot(pointslinF[:,0], pointslinF[:,1], 'o')
    plt.triplot(pointslinS[:,0], pointslinS[:,1], trilinS)
    plt.plot(pointslinS[:,0], pointslinS[:,1], 'o')
    plt.plot(pointslinI[:,0], pointslinI[:,1], 'o')
    plt.axis('equal')
    plt.show()

# lin to quad - fluid
numNodesQuadF, xquadF, yquadF, triquadF = \
    lin_to_quad.triangles(xlinF, ylinF, trilinF, tol)
pointsquadF = \
    np.column_stack((xquadF[0:numNodesQuadF:1], yquadF[0:numNodesQuadF:1]))
del xquadF, yquadF
xquadF = np.delete(pointsquadF, range(1, 2, 1), axis=1).flatten()
yquadF = np.delete(pointsquadF, range(0, 1, 1), axis=1).flatten()
# check whether we have duplicate nodes
lin_to_quad.sanityCheckTriangles(xlinF, ylinF, xquadF, yquadF, tol)

# lin to quad - solid
numNodesQuadS, xquadS, yquadS, triquadS = \
    lin_to_quad.triangles(xlinS, ylinS, trilinS, tol)
pointsquadS = \
    np.column_stack((xquadS[0:numNodesQuadS:1], yquadS[0:numNodesQuadS:1]))
del xquadS, yquadS
xquadS = np.delete(pointsquadS, range(1, 2, 1), axis=1).flatten()
yquadS = np.delete(pointsquadS, range(0, 1, 1), axis=1).flatten()
# check whether we have duplicate nodes
lin_to_quad.sanityCheckTriangles(xlinS, ylinS, xquadS, yquadS, tol)

# plot Delaunay triangulation
if showmeshesF:
    tsttriF = np.delete(triquadF, range(3, 6, 1), axis=1)
    tsttriS = np.delete(triquadS, range(3, 6, 1), axis=1)
    fig = plt.figure()
    plt.triplot(xquadF, yquadF, tsttriF)
    plt.plot(xquadF, yquadF, 'o')
    plt.triplot(xquadS, yquadS, tsttriS)
    plt.plot(xquadS, yquadS, 'o')
    plt.plot(pointsquadI[:,0], pointsquadI[:,1], 'o')
    plt.axis('equal')
    plt.show()

################################################################################
# boundary patches - fluid
################################################################################
maxNumBndryPatchesF = int(2 * nx + 4 * ny + nc)
bndryPatchesLinF    = np.zeros((maxNumBndryPatchesF, 4), dtype=int)
bndryPatchesQuadF   = np.zeros((maxNumBndryPatchesF, 5), dtype=int)
numBndryPatchesF = 0
for i in range(0, numberOfTrianglesF, 1):
    elemID = i + 1
    n0  = trilinF[i][0]
    n1  = trilinF[i][1]
    n2  = trilinF[i][2]
    n3  = triquadF[i][3]
    n4  = triquadF[i][4]
    n5  = triquadF[i][5]
    n0x = pointslinF[n0][0]
    n0y = pointslinF[n0][1]
    n1x = pointslinF[n1][0]
    n1y = pointslinF[n1][1]
    n2x = pointslinF[n2][0]
    n2y = pointslinF[n2][1]
    # make index 1-based
    n0 += 1
    n1 += 1
    n2 += 1
    n3 += 1
    n4 += 1
    n5 += 1
    # distance from cylinder center
    d0  = np.sqrt((n0x-cx)*(n0x-cx)+(n0y-cy)*(n0y-cy))
    d1  = np.sqrt((n1x-cx)*(n1x-cx)+(n1y-cy)*(n1y-cy))
    d2  = np.sqrt((n2x-cx)*(n2x-cx)+(n2y-cy)*(n2y-cy))
    ############################################################################
    # x=0
    if ((abs(n0x) < tol) and (abs(n1x) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n0
        bndryPatchesLinF[numBndryPatchesF][2]  = n1
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_x0
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n0
        bndryPatchesQuadF[numBndryPatchesF][2] = n1
        bndryPatchesQuadF[numBndryPatchesF][3] = n3
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_x0
        numBndryPatchesF                      += 1
    if ((abs(n0x) < tol) and (abs(n2x) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n2
        bndryPatchesLinF[numBndryPatchesF][2]  = n0
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_x0
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n2
        bndryPatchesQuadF[numBndryPatchesF][2] = n0
        bndryPatchesQuadF[numBndryPatchesF][3] = n4
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_x0
        numBndryPatchesF                      += 1
    if ((abs(n1x) < tol) and (abs(n2x) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n1
        bndryPatchesLinF[numBndryPatchesF][2]  = n2
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_x0
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n1
        bndryPatchesQuadF[numBndryPatchesF][2] = n2
        bndryPatchesQuadF[numBndryPatchesF][3] = n5
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_x0
        numBndryPatchesF                      += 1
    # x=lx
    if ((abs(n0x-lx) < tol) and (abs(n1x-lx) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n0
        bndryPatchesLinF[numBndryPatchesF][2]  = n1
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_x1
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n0
        bndryPatchesQuadF[numBndryPatchesF][2] = n1
        bndryPatchesQuadF[numBndryPatchesF][3] = n3
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_x1
        numBndryPatchesF                      += 1
    if ((abs(n0x-lx) < tol) and (abs(n2x-lx) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n2
        bndryPatchesLinF[numBndryPatchesF][2]  = n0
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_x1
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n2
        bndryPatchesQuadF[numBndryPatchesF][2] = n0
        bndryPatchesQuadF[numBndryPatchesF][3] = n4
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_x1
        numBndryPatchesF                      += 1
    if ((abs(n1x-lx) < tol) and (abs(n2x-lx) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n1
        bndryPatchesLinF[numBndryPatchesF][2]  = n2
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_x1
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n1
        bndryPatchesQuadF[numBndryPatchesF][2] = n2
        bndryPatchesQuadF[numBndryPatchesF][3] = n5
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_x1
        numBndryPatchesF                      += 1
    # y=0
    if ((abs(n0y) < tol) and (abs(n1y) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n0
        bndryPatchesLinF[numBndryPatchesF][2]  = n1
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_y0
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n0
        bndryPatchesQuadF[numBndryPatchesF][2] = n1
        bndryPatchesQuadF[numBndryPatchesF][3] = n3
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_y0
        numBndryPatchesF                      += 1
    if ((abs(n0y) < tol) and (abs(n2y) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n2
        bndryPatchesLinF[numBndryPatchesF][2]  = n0
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_y0
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n2
        bndryPatchesQuadF[numBndryPatchesF][2] = n0
        bndryPatchesQuadF[numBndryPatchesF][3] = n4
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_y0
        numBndryPatchesF                      += 1
    if ((abs(n1y) < tol) and (abs(n2y) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n1
        bndryPatchesLinF[numBndryPatchesF][2]  = n2
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_y0
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n1
        bndryPatchesQuadF[numBndryPatchesF][2] = n2
        bndryPatchesQuadF[numBndryPatchesF][3] = n5
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_y0
        numBndryPatchesF                      += 1
    # y=ly
    if ((abs(n0y-ly) < tol) and (abs(n1y-ly) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n0
        bndryPatchesLinF[numBndryPatchesF][2]  = n1
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_y1
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n0
        bndryPatchesQuadF[numBndryPatchesF][2] = n1
        bndryPatchesQuadF[numBndryPatchesF][3] = n3
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_y1
        numBndryPatchesF                      += 1
    if ((abs(n0y-ly) < tol) and (abs(n2y-ly) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n2
        bndryPatchesLinF[numBndryPatchesF][2]  = n0
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_y1
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n2
        bndryPatchesQuadF[numBndryPatchesF][2] = n0
        bndryPatchesQuadF[numBndryPatchesF][3] = n4
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_y1
        numBndryPatchesF                      += 1
    if ((abs(n1y-ly) < tol) and (abs(n2y-ly) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n1
        bndryPatchesLinF[numBndryPatchesF][2]  = n2
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_y1
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n1
        bndryPatchesQuadF[numBndryPatchesF][2] = n2
        bndryPatchesQuadF[numBndryPatchesF][3] = n5
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_y1
        numBndryPatchesF                      += 1
    # cylinder surface
    if ((abs(d0-cr) < tol) and (abs(d1-cr) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n0
        bndryPatchesLinF[numBndryPatchesF][2]  = n1
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_cy
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n0
        bndryPatchesQuadF[numBndryPatchesF][2] = n1
        bndryPatchesQuadF[numBndryPatchesF][3] = n3
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_cy
        numBndryPatchesF                      += 1
    if ((abs(d0-cr) < tol) and (abs(d2-cr) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n2
        bndryPatchesLinF[numBndryPatchesF][2]  = n0
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_cy
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n2
        bndryPatchesQuadF[numBndryPatchesF][2] = n0
        bndryPatchesQuadF[numBndryPatchesF][3] = n4
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_cy
        numBndryPatchesF                      += 1
    if ((abs(d1-cr) < tol) and (abs(d2-cr) < tol)):
        bndryPatchesLinF[numBndryPatchesF][0]  = elemID
        bndryPatchesLinF[numBndryPatchesF][1]  = n1
        bndryPatchesLinF[numBndryPatchesF][2]  = n2
        bndryPatchesLinF[numBndryPatchesF][3]  = patchF_cy
        bndryPatchesQuadF[numBndryPatchesF][0] = elemID
        bndryPatchesQuadF[numBndryPatchesF][1] = n1
        bndryPatchesQuadF[numBndryPatchesF][2] = n2
        bndryPatchesQuadF[numBndryPatchesF][3] = n5
        bndryPatchesQuadF[numBndryPatchesF][4] = patchF_cy
        numBndryPatchesF                      += 1
bndryPatchesLinF = np.delete(bndryPatchesLinF, \
    range(numBndryPatchesF, maxNumBndryPatchesF, 1), axis=0)
bndryPatchesQuadF = np.delete(bndryPatchesQuadF, \
    range(numBndryPatchesF, maxNumBndryPatchesF, 1), axis=0)
initpresF = np.zeros((numNodesLinF),     dtype=int)
initvelF  = np.zeros((numNodesQuadF, 2), dtype=int)
initwelF  = np.zeros((numNodesQuadF, 2), dtype=int)
# transfer boundary patch labels to initial pressure field
for i in range(0, numBndryPatchesF, 1):
    for j in range(1, 3, 1):
        nn            = bndryPatchesLinF[i][j] - 1
        initpresF[nn] = bndryPatchesLinF[i][3]
# transfer boundary patch labels to initial velocity field
for i in range(0, numBndryPatchesF, 1):
    for j in range(1, 4, 1):
        nn              = bndryPatchesQuadF[i][j] - 1
        initvelF[nn][0] = bndryPatchesQuadF[i][4]

################################################################################
# boundary patches - solid
################################################################################
bndrySnodes         = np.arange(1, 1+nc, 1)
elemID              = 0
numBndryPatchesS    = 0
bndryPatchesLinS    = np.zeros((int(nc), 4), dtype=int)
bndryPatchesQuadS   = np.zeros((int(nc), 5), dtype=int)
for i in range(0, numberOfTrianglesS, 1):
    elemID  = i + 1
    n0      = trilinS[i][0]
    n1      = trilinS[i][1]
    n2      = trilinS[i][2]
    n3      = triquadS[i][3]
    n4      = triquadS[i][4]
    n5      = triquadS[i][5]
    # make index 1-based
    n0 += 1
    n1 += 1
    n2 += 1
    n3 += 1
    n4 += 1
    n5 += 1
    # check if at least two nodes are on boundary (boundary node numbers are 1:nc)
    isBndryNodeS0 = np.any(bndrySnodes==n0)
    isBndryNodeS1 = np.any(bndrySnodes==n1)
    isBndryNodeS2 = np.any(bndrySnodes==n2)
    if (isBndryNodeS0 and isBndryNodeS1):
        bndryPatchesLinS[numBndryPatchesS][0]   = elemID
        bndryPatchesLinS[numBndryPatchesS][1]   = n0
        bndryPatchesLinS[numBndryPatchesS][2]   = n1
        bndryPatchesLinS[numBndryPatchesS][3]   = 1
        bndryPatchesQuadS[numBndryPatchesS][0]  = elemID
        bndryPatchesQuadS[numBndryPatchesS][1]  = n0
        bndryPatchesQuadS[numBndryPatchesS][2]  = n1
        bndryPatchesQuadS[numBndryPatchesS][3]  = n3
        bndryPatchesQuadS[numBndryPatchesS][4]  = 1
        numBndryPatchesS                       += 1
    elif (isBndryNodeS0 and isBndryNodeS2):
        bndryPatchesLinS[numBndryPatchesS][0]   = elemID
        bndryPatchesLinS[numBndryPatchesS][1]   = n2
        bndryPatchesLinS[numBndryPatchesS][2]   = n0
        bndryPatchesLinS[numBndryPatchesS][3]   = 1
        bndryPatchesQuadS[numBndryPatchesS][0]  = elemID
        bndryPatchesQuadS[numBndryPatchesS][1]  = n2
        bndryPatchesQuadS[numBndryPatchesS][2]  = n0
        bndryPatchesQuadS[numBndryPatchesS][3]  = n4
        bndryPatchesQuadS[numBndryPatchesS][4]  = 1
        numBndryPatchesS                       += 1
    elif (isBndryNodeS1 and isBndryNodeS2):
        bndryPatchesLinS[numBndryPatchesS][0]   = elemID
        bndryPatchesLinS[numBndryPatchesS][1]   = n1
        bndryPatchesLinS[numBndryPatchesS][2]   = n2
        bndryPatchesLinS[numBndryPatchesS][3]   = 1
        bndryPatchesQuadS[numBndryPatchesS][0]  = elemID
        bndryPatchesQuadS[numBndryPatchesS][1]  = n1
        bndryPatchesQuadS[numBndryPatchesS][2]  = n2
        bndryPatchesQuadS[numBndryPatchesS][3]  = n5
        bndryPatchesQuadS[numBndryPatchesS][4]  = 1
        numBndryPatchesS                       += 1
if (int(nc) != numBndryPatchesS):
    print(">>>ERROR: Found too many boundary patches (solid).")

################################################################################
# export meshes
################################################################################
# make triangle node IDs 1-based
trilinF  += 1
triquadF += 1
trilinS  += 1
triquadS += 1
print("Export:")
################################################################################
# export initial pressure
fname = dataprefixF + "FluidPres-0.D"
#fname = dataprefixF + "SolidPres-0.D"
print("    "+fname)
myheader = str(int(numNodesLinF)) + " 1"
np.savetxt(fname, \
    initpresF, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# export initial velocity
fname = dataprefixF + "Vel-0.D"
#fname = dataprefixF + "SolVel-0.D"
print("    "+fname)
myheader = str(int(numNodesQuadF)) + " 2"
np.savetxt(fname, \
    initvelF, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# export initial velocity
fname = dataprefixF + "Wel-0.D"
#fname = dataprefixF + "Disp-0.D"
print("    "+fname)
myheader = str(int(numNodesQuadF)) + " 2"
np.savetxt(fname, \
    initwelF, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# initial geom quad
fname = dataprefixF + "FluidSpace-0.D"
#fname = dataprefixF + "SolidSpace-0.D"
print("    "+fname)
myheader = str(int(numNodesQuadF)) + " 2"
np.savetxt(fname, \
    pointsquadF, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
################################################################################
# export X/T/B files
# geom lin
fname = fileprefixF + "_lin_FE.X"
print("    "+fname)
myheader = str(int(numNodesLinF)) + " 2"
np.savetxt(fname, \
    pointslinF, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixS + "_lin_FE.X"
print("    "+fname)
myheader = str(int(numNodesLinS)) + " 2"
np.savetxt(fname, \
    pointslinS, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixI + "_lin_FE.X"
print("    "+fname)
myheader = str(int(numNodesLinI)) + " 2"
np.savetxt(fname, \
    pointslinI, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# topo lin
fname = fileprefixF + "_lin_FE.T"
print("    "+fname)
myheader = str(int(numberOfTrianglesF)) + " " + str(int(numNodesLinF))
np.savetxt(fname, \
    trilinF, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixS + "_lin_FE.T"
print("    "+fname)
myheader = str(int(numberOfTrianglesS)) + " " + str(int(numNodesLinS))
np.savetxt(fname, \
    trilinS, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixI + "_lin_FE.T"
print("    "+fname)
myheader = str(int(nc)) + " " + str(int(numNodesLinI))
np.savetxt(fname, \
    linesLinI, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# bndry lin
fname = fileprefixF + "_lin_FE.B"
print("    "+fname)
myheader = str(int(numBndryPatchesF))
np.savetxt(fname, \
    bndryPatchesLinF, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixS + "_lin_FE.B"
print("    "+fname)
myheader = str(int(numBndryPatchesS))
np.savetxt(fname, \
    bndryPatchesLinS, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# geom quad
fname = fileprefixF + "_quad_FE.X"
print("    "+fname)
myheader = str(int(numNodesQuadF)) + " 2"
np.savetxt(fname, \
    pointsquadF, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixS + "_quad_FE.X"
print("    "+fname)
myheader = str(int(numNodesQuadS)) + " 2"
np.savetxt(fname, \
    pointsquadS, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixI + "_quad_FE.X"
print("    "+fname)
myheader = str(int(numNodesQuadI)) + " 2"
np.savetxt(fname, \
    pointsquadI, \
    fmt="%.16f", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# topo quad
fname = fileprefixF + "_quad_FE.T"
print("    "+fname)
myheader = str(int(numberOfTrianglesF)) + " " + str(int(numNodesQuadF))
np.savetxt(fname, \
    triquadF, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixS + "_quad_FE.T"
print("    "+fname)
myheader = str(int(numberOfTrianglesS)) + " " + str(int(numNodesQuadS))
np.savetxt(fname, \
    triquadS, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixI + "_quad_FE.T"
print("    "+fname)
myheader = str(int(nc)) + " " + str(int(numNodesQuadI))
np.savetxt(fname, \
    linesQuadI, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
# bndry quad
fname = fileprefixF + "_quad_FE.B"
print("    "+fname)
myheader = str(int(numBndryPatchesF))
np.savetxt(fname, \
    bndryPatchesQuadF, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
fname = fileprefixS + "_quad_FE.B"
print("    "+fname)
myheader = str(int(numBndryPatchesS))
np.savetxt(fname, \
    bndryPatchesQuadS, \
    fmt="%i", delimiter=" ", newline="\n", \
    header=myheader, footer="", comments="")
