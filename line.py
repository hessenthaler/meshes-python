#!/bin/python
################################################################################
# 1D MESH GENERATOR
################################################################################
# export format: X/T/B files (CHeart)
################################################################################
#
# node numbers:     1-------2
#
#                   1 - 3 - 2
#
################################################################################
# author: Andreas Hessenthaler
#
# developed for Python 3.5.2
# no redistribution, for personal use only
################################################################################

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys

def main():
    
    if (len(sys.argv) < 5):
        print("missing or invalid commandline arguments:")
        print("    python3 line.py filenameprefix lengthX numX originX (originY)")
        return
    
    fileprefix = str(sys.argv[1])
    
    tol = 1.0e-6
    
    fnameXl = fileprefix+"_lin_FE.X"
    fnameTl = fileprefix+"_lin_FE.T"
    fnameBl = fileprefix+"_lin_FE.B"
    fnameXq = fileprefix+"_quad_FE.X"
    fnameTq = fileprefix+"_quad_FE.T"
    fnameBq = fileprefix+"_quad_FE.B"
    
    # domain extents
    lx = float(sys.argv[2])
    
    # subdivision of lx
    nx = float(sys.argv[3])
    
    # origin at left end of line segement
    x0 = float(sys.argv[4])
    if (len(sys.argv)==6):
        y0 = float(sys.argv[5])
    
    # spacing
    dx = lx / nx
    
    # BC patches
    patch_x0 = 1
    patch_x1 = 2
    
    print("compute meshgrid")
    numberOfElements        = int(nx)
    numberOfBoundaryPatches = 2
    numberOfNodesL          = int(nx+1)
    numberOfNodesQ          = int(2*nx+1)
    print(" number of elements   : "+str(numberOfElements))
    print(" number of BC patches : "+str(numberOfBoundaryPatches))
    print(" number of nodes-l    : "+str(numberOfNodesL))
    print(" number of nodes-q    : "+str(numberOfNodesQ))
    print(" ")
    # node IDs
    print("set node numbers")
    nodeNumbersL = np.zeros((numberOfNodesL), dtype=int)
    nodeNumbersQ = np.zeros((numberOfNodesQ), dtype=int)
    nodeNumber = 0
    for i in range(0, int(nx)+1, 1):
        nodeNumber +=1
        nodeNumbersL[i]     = nodeNumber
        nodeNumbersQ[2*i]   = nodeNumber
    if (nodeNumber != numberOfNodesL):
        print(">>>ERROR: invalid number of nodes-l")
    for i in range(0, int(nx), 1):
        nodeNumber +=1
        nodeNumbersQ[2*i+1] = nodeNumber
    if (nodeNumber != numberOfNodesQ):
        print(">>>ERROR: invalid number of nodes-q")
    print(" ")
    # node coordinates
    print("set node positions")
    xl  = np.zeros((numberOfNodesL), dtype=float)
    xq  = np.zeros((numberOfNodesQ), dtype=float)
    if (len(sys.argv)==6):
        yl  = np.zeros((numberOfNodesL), dtype=float)
        yq  = np.zeros((numberOfNodesQ), dtype=float)
        yl += y0
        yq += y0
    nodeNumber = 0
    for i in range(0, int(nx)+1, 1):
        nodeNumber +=1
        xl[i] = x0 + i * dx
        xq[i] = x0 + i * dx
    if (nodeNumber != numberOfNodesL):
        print(">>>ERROR: invalid number of nodes-l")
    if (abs(xl[numberOfNodesL-1]-lx) > tol):
        print(">>>ERROR: invalid node-l position at x = lx")
    for i in range(0, int(nx), 1):
        nodeNumber +=1
        xq[numberOfNodesL+i]   = x0 + 0.5 * dx + i * dx
    if (nodeNumber != numberOfNodesQ):
        print(">>>ERROR: invalid number of nodes-q")
    if (abs(xq[numberOfNodesL-1]-lx) > tol):
        print(">>>ERROR: invalid node-q position at x = lx")
    print(" ")
    # element connectivity
    print("compute connectivity")
    elementNumbersL = np.zeros((numberOfElements, 2), dtype=int)
    elementNumbersQ = np.zeros((numberOfElements, 3), dtype=int)
    elementNumber = 0
    for i in range(0, int(nx), 1):
        elementNumbersL[elementNumber][0] = nodeNumbersL[i  ]
        elementNumbersL[elementNumber][1] = nodeNumbersL[i+1]
        elementNumbersQ[elementNumber][0] = nodeNumbersQ[2*i  ]
        elementNumbersQ[elementNumber][1] = nodeNumbersQ[2*i+1]
        elementNumbersQ[elementNumber][2] = nodeNumbersQ[2*i+2]
        elementNumber += 1
    print(" ")
    # boundary elements
    print("set boundary patches")
    boundaryElementsL = np.zeros((numberOfBoundaryPatches, 3), dtype=int)
    boundaryElementsQ = np.zeros((numberOfBoundaryPatches, 3), dtype=int)
    boundaryElementsL[0][0] = 1
    boundaryElementsL[0][1] = 1
    boundaryElementsL[0][2] = patch_x0
    boundaryElementsL[1][0] = nx
    boundaryElementsL[1][1] = numberOfNodesL
    boundaryElementsL[1][2] = patch_x1
    boundaryElementsQ[0][0] = 1
    boundaryElementsQ[0][1] = 1
    boundaryElementsQ[0][2] = patch_x0
    boundaryElementsQ[1][0] = nx
    boundaryElementsQ[1][1] = numberOfNodesL
    boundaryElementsQ[1][2] = patch_x1
    print(" ")
    print("export X (lin)")
    if (len(sys.argv)==6):
        myheader = " "+str(numberOfNodesL)+" 2"
        coordinatesl = np.column_stack((xl, yl))
        np.savetxt(fnameXl, coordinatesl, fmt="%f", header=myheader, comments="")
        print("export X (quad)")
        myheader = " "+str(numberOfNodesQ)+" 2"
        coordinatesq = np.column_stack((xq, yq))
        np.savetxt(fnameXq, coordinatesq, fmt="%f", header=myheader, comments="")
    else:
        myheader = " "+str(numberOfNodesL)+" 1"
        np.savetxt(fnameXl, xl, fmt="%f", header=myheader, comments="")
        print("export X (quad)")
        myheader = " "+str(numberOfNodesQ)+" 1"
        np.savetxt(fnameXq, xq, fmt="%f", header=myheader, comments="")
    print("export T (lin)")
    myheader = " "+str(numberOfElements)+" "+str(numberOfNodesL)
    np.savetxt(fnameTl, elementNumbersL, fmt="%d", header=myheader, comments="")
    print("export T (quad)")
    myheader = " "+str(numberOfElements)+" "+str(numberOfNodesQ)
    np.savetxt(fnameTq, elementNumbersQ, fmt="%d", header=myheader, comments="")
    print("export B (lin)")
    myheader = " "+str(numberOfBoundaryPatches)+" "+str(numberOfNodesL)
    np.savetxt(fnameBl, boundaryElementsL, fmt="%d", header=myheader, comments="")
    print("export B (quad)")
    myheader = " "+str(numberOfBoundaryPatches)+" "+str(numberOfNodesQ)
    np.savetxt(fnameBq, boundaryElementsQ, fmt="%d", header=myheader, comments="")
#    print(" ")
#    print("show mesh")
#    fig, ax = plt.subplots()
#    ax.scatter(xl, xl*0.0)
#    ax.scatter(xq, xq*0.0+0.1)
#    plt.show()

if __name__ == "__main__":
    main()
