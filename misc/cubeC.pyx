# cython: profile=False
import numpy
import string
import sys
cimport numpy
cimport cython
from numpy cimport ndarray
from libc.stdio cimport *
from libc.math cimport sqrt
from libc.math cimport abs
from libc.math cimport pow

sys.dont_write_bytecode = True

INTTYPE = numpy.int
DOUBLETYPE = numpy.double
ctypedef numpy.int_t INTTYPE_t
ctypedef numpy.double_t DOUBLETYPE_t

def sanityCheckUniqueNodes(ndarray[numpy.double_t, ndim=1]    xLin, \
                           ndarray[numpy.double_t, ndim=1]    yLin, \
                           ndarray[numpy.double_t, ndim=1]    zLin, \
                           ndarray[numpy.double_t, ndim=1]    xQuad, \
                           ndarray[numpy.double_t, ndim=1]    yQuad, \
                           ndarray[numpy.double_t, ndim=1]    zQuad, \
                           double                             tol):
    
    cdef unsigned int   nonUniqueNodesLin, nonUniqueNodesQuad, i, j
    cdef double         myix, myiy, myiz, myjx, myjy, myjz
    
    # sanity check nodes-lin
    nonUniqueNodesLin = 0
    for i in range(0, xLin.shape[0], 1):
        for j in range(i+1, xLin.shape[0], 1):
            myix = xLin[i]
            myiy = yLin[i]
            myiz = zLin[i]
            myjx = xLin[j]
            myjy = yLin[j]
            myjz = zLin[j]
            if (    (abs(myix - myjx) < tol) \
                and (abs(myiy - myjy) < tol) \
                and (abs(myiz - myjz) < tol)):
                nonUniqueNodesLin += 1
                break
    if (nonUniqueNodesLin != 0):
        print(">>>ERROR: number of non-unique nodes-l: ",nonUniqueNodesLin)
        return -1
    # sanity check nodes-quad
    nonUniqueNodesQuad = 0
    for i in range(0, xQuad.shape[0], 1):
        for j in range(i+1, xQuad.shape[0], 1):
            myix = xQuad[i]
            myiy = yQuad[i]
            myiz = zQuad[i]
            myjx = xQuad[j]
            myjy = yQuad[j]
            myjz = zQuad[j]
            if (    (abs(myix - myjx) < tol) \
                and (abs(myiy - myjy) < tol) \
                and (abs(myiz - myjz) < tol)):
                nonUniqueNodesQuad += 1
                print(" %i %i %f %f %f %f %f %f" % (i, j, myix, myiy, myiz, myjx, myjy, myjz))
                break
    if (nonUniqueNodesQuad != 0):
        print(">>>ERROR: number of non-unique nodes-q: ",nonUniqueNodesQuad)
        return -1
    return 0

def lin2quad(ndarray[numpy.double_t, ndim=1]    pointsLin, \
             ndarray[numpy.int_t,    ndim=2]    tetsLin, \
             ndarray[numpy.int_t,    ndim=2]    tetsQuad, \
             unsigned int                       numberOfNodesLin, \
             unsigned int                       numberOfTets, \
             unsigned int                       numberOfBoundaryElements, \
             double                             x0, \
             double                             x1, \
             double                             y0, \
             double                             y1, \
             double                             z0, \
             double                             z1, \
             unsigned int                       patch_x0, \
             unsigned int                       patch_x1, \
             unsigned int                       patch_y0, \
             unsigned int                       patch_y1, \
             unsigned int                       patch_z0, \
             unsigned int                       patch_z1, \
             double                             tol):

    cdef unsigned int                       numberOfNodesQuad, maxNumberOfNodesQuad, boundaryElementIter
    cdef unsigned int                       id4, id5, id6, id7, id8, id9, i, j
    cdef double                             nx0, ny0, nz0, nx1, ny1, nz1, nx2, ny2, nz2, nx3, ny3, nz3
    cdef double                             tstx, tsty, tstz, d4, d5, d6, d7, d8, d9
    cdef double                             mx4, my4, mz4, mx5, my5, mz5, mx6, my6, mz6, mx7, my7, mz7, mx8, my8, mz8, mx9, my9, mz9
    cdef bint                               new4, new5, new6, new7, new8, new9
    cdef ndarray[numpy.double_t, ndim=1]    pointsQuad
    cdef ndarray[numpy.int_t,    ndim=2]    boundaryElementsLin, boundaryElementsQuad
    
    # allocate memory for boundary element connectivity
    boundaryElementsLin  = numpy.zeros((numberOfBoundaryElements, 5)).astype(int)
    boundaryElementsQuad = numpy.zeros((numberOfBoundaryElements, 8)).astype(int)
    # allocate array to store all quad-nodes (max size, temp array)
    numberOfNodesQuad    = numberOfNodesLin
    maxNumberOfNodesQuad = numberOfTets * 10
    pointsQuad           = numpy.zeros((maxNumberOfNodesQuad*3)).astype(float)
    # copy lin-nodes to quad-nodes
    for i in range(0, numberOfNodesLin, 1):
        pointsQuad[0*maxNumberOfNodesQuad+i] = pointsLin[0*numberOfNodesLin+i]
        pointsQuad[1*maxNumberOfNodesQuad+i] = pointsLin[1*numberOfNodesLin+i]
        pointsQuad[2*maxNumberOfNodesQuad+i] = pointsLin[2*numberOfNodesLin+i]
    # compute new quad-nodes
    boundaryElementIter = 0
    for i in range(0, numberOfTets, 1):
        # get node coordinates for all nodes of a given tet
        nx0 = pointsLin[0*numberOfNodesLin+tetsLin[i][0]-1]
        ny0 = pointsLin[1*numberOfNodesLin+tetsLin[i][0]-1]
        nz0 = pointsLin[2*numberOfNodesLin+tetsLin[i][0]-1]
        nx1 = pointsLin[0*numberOfNodesLin+tetsLin[i][1]-1]
        ny1 = pointsLin[1*numberOfNodesLin+tetsLin[i][1]-1]
        nz1 = pointsLin[2*numberOfNodesLin+tetsLin[i][1]-1]
        nx2 = pointsLin[0*numberOfNodesLin+tetsLin[i][2]-1]
        ny2 = pointsLin[1*numberOfNodesLin+tetsLin[i][2]-1]
        nz2 = pointsLin[2*numberOfNodesLin+tetsLin[i][2]-1]
        nx3 = pointsLin[0*numberOfNodesLin+tetsLin[i][3]-1]
        ny3 = pointsLin[1*numberOfNodesLin+tetsLin[i][3]-1]
        nz3 = pointsLin[2*numberOfNodesLin+tetsLin[i][3]-1]
        # new nodes
        mx4 = 0.5 * (nx0 + nx1)
        my4 = 0.5 * (ny0 + ny1)
        mz4 = 0.5 * (nz0 + nz1)
        mx5 = 0.5 * (nx0 + nx2)
        my5 = 0.5 * (ny0 + ny2)
        mz5 = 0.5 * (nz0 + nz2)
        mx6 = 0.5 * (nx1 + nx2)
        my6 = 0.5 * (ny1 + ny2)
        mz6 = 0.5 * (nz1 + nz2)
        mx7 = 0.5 * (nx0 + nx3)
        my7 = 0.5 * (ny0 + ny3)
        mz7 = 0.5 * (nz0 + nz3)
        mx8 = 0.5 * (nx1 + nx3)
        my8 = 0.5 * (ny1 + ny3)
        mz8 = 0.5 * (nz1 + nz3)
        mx9 = 0.5 * (nx2 + nx3)
        my9 = 0.5 * (ny2 + ny3)
        mz9 = 0.5 * (nz2 + nz3)
        new4 = True
        new5 = True
        new6 = True
        new7 = True
        new8 = True
        new9 = True
        for j in range(numberOfNodesLin, numberOfNodesQuad, 1):
            if (not new4):
                if (not new5):
                    if (not new6):
                        if (not new7):
                            if (not new8):
                                if (not new9):
                                    break
            tstx = pointsQuad[0*maxNumberOfNodesQuad+j]
            tsty = pointsQuad[1*maxNumberOfNodesQuad+j]
            tstz = pointsQuad[2*maxNumberOfNodesQuad+j]
            if new4:
                d4 = sqrt((tstx-mx4)**2.0 + (tsty-my4)**2.0 + (tstz-mz4)**2.0)
                if (d4 < tol):
                    new4 = False
                    id4  = j+1
            if new5:
                d5 = sqrt((tstx-mx5)**2.0 + (tsty-my5)**2.0 + (tstz-mz5)**2.0)
                if (d5 < tol):
                    new5 = False
                    id5  = j+1
            if new6:
                d6 = sqrt((tstx-mx6)**2.0 + (tsty-my6)**2.0 + (tstz-mz6)**2.0)
                if (d6 < tol):
                    new6 = False
                    id6  = j+1
            if new7:
                d7 = sqrt((tstx-mx7)**2.0 + (tsty-my7)**2.0 + (tstz-mz7)**2.0)
                if (d7 < tol):
                    new7 = False
                    id7  = j+1
            if new8:
                d8 = sqrt((tstx-mx8)**2.0 + (tsty-my8)**2.0 + (tstz-mz8)**2.0)
                if (d8 < tol):
                    new8 = False
                    id8  = j+1
            if new9:
                d9 = sqrt((tstx-mx9)**2.0 + (tsty-my9)**2.0 + (tstz-mz9)**2.0)
                if (d9 < tol):
                    new9 = False
                    id9  = j+1
        if new4:
            id4 = numberOfNodesQuad + 1
            pointsQuad[0*maxNumberOfNodesQuad+numberOfNodesQuad] = mx4
            pointsQuad[1*maxNumberOfNodesQuad+numberOfNodesQuad] = my4
            pointsQuad[2*maxNumberOfNodesQuad+numberOfNodesQuad] = mz4
            numberOfNodesQuad += 1
        if new5:
            id5 = numberOfNodesQuad + 1
            pointsQuad[0*maxNumberOfNodesQuad+numberOfNodesQuad] = mx5
            pointsQuad[1*maxNumberOfNodesQuad+numberOfNodesQuad] = my5
            pointsQuad[2*maxNumberOfNodesQuad+numberOfNodesQuad] = mz5
            numberOfNodesQuad += 1
        if new6:
            id6 = numberOfNodesQuad + 1
            pointsQuad[0*maxNumberOfNodesQuad+numberOfNodesQuad] = mx6
            pointsQuad[1*maxNumberOfNodesQuad+numberOfNodesQuad] = my6
            pointsQuad[2*maxNumberOfNodesQuad+numberOfNodesQuad] = mz6
            numberOfNodesQuad += 1
        if new7:
            id7 = numberOfNodesQuad + 1
            pointsQuad[0*maxNumberOfNodesQuad+numberOfNodesQuad] = mx7
            pointsQuad[1*maxNumberOfNodesQuad+numberOfNodesQuad] = my7
            pointsQuad[2*maxNumberOfNodesQuad+numberOfNodesQuad] = mz7
            numberOfNodesQuad += 1
        if new8:
            id8 = numberOfNodesQuad + 1
            pointsQuad[0*maxNumberOfNodesQuad+numberOfNodesQuad] = mx8
            pointsQuad[1*maxNumberOfNodesQuad+numberOfNodesQuad] = my8
            pointsQuad[2*maxNumberOfNodesQuad+numberOfNodesQuad] = mz8
            numberOfNodesQuad += 1
        if new9:
            id9 = numberOfNodesQuad + 1
            pointsQuad[0*maxNumberOfNodesQuad+numberOfNodesQuad] = mx9
            pointsQuad[1*maxNumberOfNodesQuad+numberOfNodesQuad] = my9
            pointsQuad[2*maxNumberOfNodesQuad+numberOfNodesQuad] = mz9
            numberOfNodesQuad += 1
        # copy node numbers from lin-tet to quad-tet
        for j in range(0, 4, 1):
            tetsQuad[i][j] = tetsLin[i][j]
        # assign new node numbers
        tetsQuad[i][4] = id4
        tetsQuad[i][5] = id5
        tetsQuad[i][6] = id6
        tetsQuad[i][7] = id7
        tetsQuad[i][8] = id8
        tetsQuad[i][9] = id9
        # get boundary elements
        # x = x0
        if ((abs(nx0 - x0) < tol) and (abs(nx1 - x0) < tol) and (abs(nx2 - x0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][4] = patch_x0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x0
            boundaryElementIter += 1
        if ((abs(nx0 - x0) < tol) and (abs(nx1 - x0) < tol) and (abs(nx3 - x0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_x0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x0
            boundaryElementIter += 1
        if ((abs(nx0 - x0) < tol) and (abs(nx2 - x0) < tol) and (abs(nx3 - x0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_x0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x0
            boundaryElementIter += 1
        if ((abs(nx1 - x0) < tol) and (abs(nx2 - x0) < tol) and (abs(nx3 - x0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_x0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x0
            boundaryElementIter += 1
        # x = x1
        if ((abs(nx0 - x1) < tol) and (abs(nx1 - x1) < tol) and (abs(nx2 - x1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][4] = patch_x1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x1
            boundaryElementIter += 1
        if ((abs(nx0 - x1) < tol) and (abs(nx1 - x1) < tol) and (abs(nx3 - x1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_x1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x1
            boundaryElementIter += 1
        if ((abs(nx0 - x1) < tol) and (abs(nx2 - x1) < tol) and (abs(nx3 - x1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_x1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x1
            boundaryElementIter += 1
        if ((abs(nx1 - x1) < tol) and (abs(nx2 - x1) < tol) and (abs(nx3 - x1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_x1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][7] = patch_x1
            boundaryElementIter += 1
        # y = y0
        if ((abs(ny0 - y0) < tol) and (abs(ny1 - y0) < tol) and (abs(ny2 - y0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][4] = patch_y0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y0
            boundaryElementIter += 1
        if ((abs(ny0 - y0) < tol) and (abs(ny1 - y0) < tol) and (abs(ny3 - y0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_y0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y0
            boundaryElementIter += 1
        if ((abs(ny0 - y0) < tol) and (abs(ny2 - y0) < tol) and (abs(ny3 - y0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_y0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y0
            boundaryElementIter += 1
        if ((abs(ny1 - y0) < tol) and (abs(ny2 - y0) < tol) and (abs(ny3 - y0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_y0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y0
            boundaryElementIter += 1
        # y = y1
        if ((abs(ny0 - y1) < tol) and (abs(ny1 - y1) < tol) and (abs(ny2 - y1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][4] = patch_y1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y1
            boundaryElementIter += 1
        if ((abs(ny0 - y1) < tol) and (abs(ny1 - y1) < tol) and (abs(ny3 - y1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_y1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y1
            boundaryElementIter += 1
        if ((abs(ny0 - y1) < tol) and (abs(ny2 - y1) < tol) and (abs(ny3 - y1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_y1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y1
            boundaryElementIter += 1
        if ((abs(ny1 - y1) < tol) and (abs(ny2 - y1) < tol) and (abs(ny3 - y1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_y1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][7] = patch_y1
            boundaryElementIter += 1
        # z = z0
        if ((abs(nz0 - z0) < tol) and (abs(nz1 - z0) < tol) and (abs(nz2 - z0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][4] = patch_z0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z0
            boundaryElementIter += 1
        if ((abs(nz0 - z0) < tol) and (abs(nz1 - z0) < tol) and (abs(nz3 - z0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_z0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z0
            boundaryElementIter += 1
        if ((abs(nz0 - z0) < tol) and (abs(nz2 - z0) < tol) and (abs(nz3 - z0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_z0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z0
            boundaryElementIter += 1
        if ((abs(nz1 - z0) < tol) and (abs(nz2 - z0) < tol) and (abs(nz3 - z0) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_z0
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z0
            boundaryElementIter += 1
        # z = z1
        if ((abs(nz0 - z1) < tol) and (abs(nz1 - z1) < tol) and (abs(nz2 - z1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][4] = patch_z1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z1
            boundaryElementIter += 1
        if ((abs(nz0 - z1) < tol) and (abs(nz1 - z1) < tol) and (abs(nz3 - z1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_z1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][4]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z1
            boundaryElementIter += 1
        if ((abs(nz0 - z1) < tol) and (abs(nz2 - z1) < tol) and (abs(nz3 - z1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][0]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_z1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][0]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][5]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][7]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z1
            boundaryElementIter += 1
        if ((abs(nz1 - z1) < tol) and (abs(nz2 - z1) < tol) and (abs(nz3 - z1) < tol)):
            boundaryElementsLin[boundaryElementIter][0] = i + 1
            boundaryElementsLin[boundaryElementIter][1] = tetsLin[i][1]
            boundaryElementsLin[boundaryElementIter][2] = tetsLin[i][2]
            boundaryElementsLin[boundaryElementIter][3] = tetsLin[i][3]
            boundaryElementsLin[boundaryElementIter][4] = patch_z1
            boundaryElementsQuad[boundaryElementIter][0] = i + 1
            boundaryElementsQuad[boundaryElementIter][1] = tetsQuad[i][1]
            boundaryElementsQuad[boundaryElementIter][2] = tetsQuad[i][2]
            boundaryElementsQuad[boundaryElementIter][3] = tetsQuad[i][3]
            boundaryElementsQuad[boundaryElementIter][4] = tetsQuad[i][6]
            boundaryElementsQuad[boundaryElementIter][5] = tetsQuad[i][8]
            boundaryElementsQuad[boundaryElementIter][6] = tetsQuad[i][9]
            boundaryElementsQuad[boundaryElementIter][7] = patch_z1
            boundaryElementIter += 1
    
    # sanity check for number of boundary elements
    if (numberOfBoundaryElements != boundaryElementIter):
        print(">>>ERROR: Invalid number of boundary elements")
        return
    
    return numberOfNodesQuad, maxNumberOfNodesQuad, pointsQuad, boundaryElementsLin, boundaryElementsQuad
