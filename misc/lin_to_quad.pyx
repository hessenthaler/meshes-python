# cython: profile=False
import numpy
import string
import sys
cimport numpy
cimport cython
from numpy cimport ndarray
from libc.stdio cimport *
from libc.math cimport sqrt
from libc.math cimport abs
from libc.math cimport pow

sys.dont_write_bytecode = True

INTTYPE = numpy.int
DOUBLETYPE = numpy.double
ctypedef numpy.int_t INTTYPE_t
ctypedef numpy.double_t DOUBLETYPE_t

def sanityCheckTriangles(ndarray[numpy.double_t, ndim=1]    xLin, \
                         ndarray[numpy.double_t, ndim=1]    yLin, \
                         ndarray[numpy.double_t, ndim=1]    xQuad, \
                         ndarray[numpy.double_t, ndim=1]    yQuad, \
                         double                             tol):
    
    cdef unsigned int   nonUniqueNodesLin, nonUniqueNodesQuad, i, j
    cdef double         myix, myiy, myjx, myjy
    
    # sanity check nodes-lin
    nonUniqueNodesLin = 0
    for i in range(0, xLin.shape[0], 1):
        for j in range(i+1, xLin.shape[0], 1):
            myix = xLin[i]
            myiy = yLin[i]
            myjx = xLin[j]
            myjy = yLin[j]
            if (    (abs(myix - myjx) < tol) \
                and (abs(myiy - myjy) < tol)):
                nonUniqueNodesLin += 1
                break
    if (nonUniqueNodesLin != 0):
        print(">>>ERROR: number of non-unique nodes: "+str(nonUniqueNodesLin)+" (lin)")
        return -1
    # sanity check nodes-quad
    nonUniqueNodesQuad = 0
    for i in range(0, xQuad.shape[0], 1):
        for j in range(i+1, xQuad.shape[0], 1):
            myix = xQuad[i]
            myiy = yQuad[i]
            myjx = xQuad[j]
            myjy = yQuad[j]
            if (    (abs(myix - myjx) < tol) \
                and (abs(myiy - myjy) < tol)):
                nonUniqueNodesQuad += 1
                break
    if (nonUniqueNodesQuad != 0):
        print(">>>ERROR: number of non-unique nodes: "+str(nonUniqueNodesLin)+" (quad)")
        return -1
    return 0

# trisLin  : on input  0-based node IDs
# trisQuad : on return 0-based node IDs
def triangles(ndarray[numpy.double_t, ndim=1]    xl, \
              ndarray[numpy.double_t, ndim=1]    yl, \
              ndarray[numpy.int_t,    ndim=2]    trisLin, \
              double                             tol):

    cdef unsigned int                       numberOfNodesQuad, maxNumberOfNodesQuad
    cdef unsigned int                       id0, id1, id2, id3, id4, id5
    cdef unsigned int                       i, j, numberOfNodesLin
    cdef double                             nx0, ny0, nx1, ny1, nx2, ny2
    cdef double                             tstx, tsty, d3, d4, d5
    cdef double                             mx3, my3, mx4, my4, mx5, my5
    cdef bint                               new3, new4, new5
    cdef ndarray[numpy.double_t, ndim=1]    xq, yq
    cdef ndarray[numpy.int_t,    ndim=2]    trisQuad
    
    # allocate array to store all quad-nodes (max size, temp array)
    numberOfNodesLin     = xl.shape[0]
    numberOfNodesQuad    = numberOfNodesLin
    numberOfTris         = trisLin.shape[0]
    maxNumberOfNodesQuad = numberOfTris * 6
    trisQuad             = numpy.zeros((numberOfTris, 6)).astype(int)
    xq                   = numpy.zeros((maxNumberOfNodesQuad)).astype(float)
    yq                   = numpy.zeros((maxNumberOfNodesQuad)).astype(float)
    # copy lin-nodes to quad-nodes
    for i in range(0, numberOfNodesLin, 1):
        xq[i] = xl[i]
        yq[i] = yl[i]
    # compute new quad-nodes
    for i in range(0, numberOfTris, 1):
        # get node coordinates for all nodes of a given tri
        id0 = trisLin[i][0]
        id1 = trisLin[i][1]
        id2 = trisLin[i][2]
        nx0 = xl[id0]
        ny0 = yl[id0]
        nx1 = xl[id1]
        ny1 = yl[id1]
        nx2 = xl[id2]
        ny2 = yl[id2]
        # new nodes
        mx3 = 0.5 * (nx0 + nx1)
        my3 = 0.5 * (ny0 + ny1)
        mx4 = 0.5 * (nx0 + nx2)
        my4 = 0.5 * (ny0 + ny2)
        mx5 = 0.5 * (nx1 + nx2)
        my5 = 0.5 * (ny1 + ny2)
        new3 = True
        new4 = True
        new5 = True
        for j in range(numberOfNodesLin, numberOfNodesQuad, 1):
            if (not new3):
                if (not new4):
                    if (not new5):
                        break
            tstx = xq[j]
            tsty = yq[j]
            if new3:
                d3 = sqrt((tstx-mx3)**2.0 + (tsty-my3)**2.0)
                if (d3 < tol):
                    new3 = False
                    id3  = j
            if new4:
                d4 = sqrt((tstx-mx4)**2.0 + (tsty-my4)**2.0)
                if (d4 < tol):
                    new4 = False
                    id4  = j
            if new5:
                d5 = sqrt((tstx-mx5)**2.0 + (tsty-my5)**2.0)
                if (d5 < tol):
                    new5 = False
                    id5  = j
        if new3:
            id3 = numberOfNodesQuad
            xq[numberOfNodesQuad] = mx3
            yq[numberOfNodesQuad] = my3
            numberOfNodesQuad += 1
        if new4:
            id4 = numberOfNodesQuad
            xq[numberOfNodesQuad] = mx4
            yq[numberOfNodesQuad] = my4
            numberOfNodesQuad += 1
        if new5:
            id5 = numberOfNodesQuad
            xq[numberOfNodesQuad] = mx5
            yq[numberOfNodesQuad] = my5
            numberOfNodesQuad += 1
        # copy node numbers from lin-tet to quad-tet
        for j in range(0, 3, 1):
            trisQuad[i][j] = trisLin[i][j]
        # assign new node numbers
        trisQuad[i][3] = id3
        trisQuad[i][4] = id4
        trisQuad[i][5] = id5
    
    return numberOfNodesQuad, xq, yq, trisQuad
