#!/bin/python
################################################################################
# QUADRILATERAL MESH GENERATOR
################################################################################
# export format: X/T/B files (CHeart)
################################################################################
#
# node numbers:     3-------4
#                   |       |
#                   |       |
#                   |       |
#                   1-------2
#
#                   3 - 9 - 4
#                   |       |
#                   6   7   8
#                   |       |
#                   1 - 5 - 2
#
################################################################################
# author: Andreas Hessenthaler
#
# developed for Python 2.7.6
# no redistribution, for personal use only
################################################################################

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys
from mpl_toolkits.mplot3d import Axes3D

def main():
    
    if (len(sys.argv) != 9):
        print "missing or invalid commandline arguments:"
        print "    python quadrilateral.py filenameprefix lengthX lengthY numX numY originX originY init_folder/"
        return
    
    fileprefix = str(sys.argv[1])
    
    tol = 1.0e-6
    
    fnameXl = fileprefix+"_lin_FE.X"
    fnameTl = fileprefix+"_lin_FE.T"
    fnameBl = fileprefix+"_lin_FE.B"
    fnameXq = fileprefix+"_quad_FE.X"
    fnameTq = fileprefix+"_quad_FE.T"
    fnameBq = fileprefix+"_quad_FE.B"
    
    fnameInitSpace      = str(sys.argv[8])+"SolidSpace-0.D"
    fnameInitVel        = str(sys.argv[8])+"SolVel-0.D"
    fnameInitPres       = str(sys.argv[8])+"SolidPres-0.D"
    fnameAnalyticalL    = str(sys.argv[8])+"AnalyticalSolutionL-0.D"
    fnameAnalyticalQ    = str(sys.argv[8])+"AnalyticalSolutionQ-0.D"
    
    # domain extents
    lx = float(sys.argv[2])
    ly = float(sys.argv[3])
    
    # subdivision of lx, ly
    nx = float(sys.argv[4])
    ny = float(sys.argv[5])
    
    # bottom-left coordinate
    x0 = float(sys.argv[6])
    y0 = float(sys.argv[7])
    
    # spacing
    dx = lx / nx
    dy = ly / ny
    
    # BC patches
    patch_x0 = 1
    patch_x1 = 2
    patch_y0 = 3
    patch_y1 = 4
    
    print "compute meshgrid"
    Xl, Yl = np.meshgrid(np.arange(x0, x0+lx+tol, 1.0*dx), np.arange(y0, y0+ly+tol, 1.0*dy))
    Xq, Yq = np.meshgrid(np.arange(x0, x0+lx+tol, 0.5*dx), np.arange(y0, y0+ly+tol, 0.5*dy))
    numberOfElements        = int(nx*ny)
    numberOfBoundaryPatches = int(2*nx+2*ny)
    numberOfNodesL          = int((nx+1)*(ny+1))
    numberOfNodesQ          = int((2*nx+1)*(2*ny+1))
    print " number of elements   : "+str(numberOfElements)
    print " number of BC patches : "+str(numberOfBoundaryPatches)
    print " number of nodes-l    : "+str(numberOfNodesL)
    print " number of nodes-q    : "+str(numberOfNodesQ)
    print " "
    print "set node numbers"
    nodeNumber = 0
    nodeNumbersL = 0*Xl
    nodeNumbersQ = np.zeros(Xq.shape, dtype=int)
    for i in range(0, int(ny)+1, 1):
        for j in range(0, int(nx)+1, 1):
            nodeNumber +=1
            nodeNumbersL[i][j]     = nodeNumber
            nodeNumbersQ[2*i][2*j] = nodeNumber
    for i in range(0, int(ny), 1):
        for j in range(0, int(nx), 1):
            nodeNumber +=1
            nodeNumbersQ[2*i][2*j+1] = nodeNumber
        for j in range(0, 2*int(nx)+1, 1):
            nodeNumber +=1
            nodeNumbersQ[2*i+1][j] = nodeNumber
    for j in range(0, int(nx), 1):
        nodeNumber +=1
        nodeNumbersQ[2*int(ny)][2*j+1] = nodeNumber
    print " "
    print "set node positions (lin)"
    xl = Xl.flatten()
    yl = Yl.flatten()
    coordinatesl = np.column_stack((xl, yl))
    print " "
    print "set node positions (quad)"
    xq_notsorted = Xq.flatten()
    yq_notsorted = Yq.flatten()
    xq = 0.0 * xq_notsorted
    yq = 0.0 * yq_notsorted
    flat_nodeNumbersQ = nodeNumbersQ.flatten()
    for i in range(0, numberOfNodesQ, 1):
        xq[flat_nodeNumbersQ[i]-1] = xq_notsorted[i]
        yq[flat_nodeNumbersQ[i]-1] = yq_notsorted[i]
    coordinatesq = np.column_stack((xq, yq))
    initVel      = 0.0 * coordinatesq
    print " "
    print "compute connectivity"
    elementNumbersL = np.zeros((numberOfElements, 4), dtype=int)
    elementNumbersQ = np.zeros((numberOfElements, 9), dtype=int)
    elementNumber = 0
    for i in range(0, int(ny), 1):
        for j in range(0, int(nx), 1):
                elementNumbersL[elementNumber][0] = nodeNumbersL[i  ][j  ]
                elementNumbersL[elementNumber][1] = nodeNumbersL[i  ][j+1]
                elementNumbersL[elementNumber][2] = nodeNumbersL[i+1][j  ]
                elementNumbersL[elementNumber][3] = nodeNumbersL[i+1][j+1]
                elementNumbersQ[elementNumber][0] = nodeNumbersQ[2*i  ][2*j  ]
                elementNumbersQ[elementNumber][1] = nodeNumbersQ[2*i  ][2*j+2]
                elementNumbersQ[elementNumber][2] = nodeNumbersQ[2*i+2][2*j  ]
                elementNumbersQ[elementNumber][3] = nodeNumbersQ[2*i+2][2*j+2]
                elementNumbersQ[elementNumber][4] = nodeNumbersQ[2*i  ][2*j+1]
                elementNumbersQ[elementNumber][5] = nodeNumbersQ[2*i+1][2*j  ]
                elementNumbersQ[elementNumber][6] = nodeNumbersQ[2*i+1][2*j+1]
                elementNumbersQ[elementNumber][7] = nodeNumbersQ[2*i+1][2*j+2]
                elementNumbersQ[elementNumber][8] = nodeNumbersQ[2*i+2][2*j+1]
                elementNumber += 1
    print " "
    print "set boundary patches"
    boundaryElementsL = np.zeros((numberOfBoundaryPatches, 4), dtype=int)
    boundaryElementsQ = np.zeros((numberOfBoundaryPatches, 5), dtype=int)
    boundaryElementNumber = 0
    for i in range(0, numberOfElements, 1):
        nx0 = xl[elementNumbersL[i][0]-1]
        ny0 = yl[elementNumbersL[i][0]-1]
        nx1 = xl[elementNumbersL[i][1]-1]
        ny1 = yl[elementNumbersL[i][1]-1]
        nx2 = xl[elementNumbersL[i][2]-1]
        ny2 = yl[elementNumbersL[i][2]-1]
        nx3 = xl[elementNumbersL[i][3]-1]
        ny3 = yl[elementNumbersL[i][3]-1]
        # y = y0
        if ((abs(ny0-y0) < tol) and (abs(ny1-y0) < tol)):
            boundaryElementsL[boundaryElementNumber][0] = i + 1
            boundaryElementsL[boundaryElementNumber][1] = elementNumbersL[i][0]
            boundaryElementsL[boundaryElementNumber][2] = elementNumbersL[i][1]
            boundaryElementsL[boundaryElementNumber][3] = patch_y0
            boundaryElementsQ[boundaryElementNumber][0] = i + 1
            boundaryElementsQ[boundaryElementNumber][1] = elementNumbersQ[i][0]
            boundaryElementsQ[boundaryElementNumber][2] = elementNumbersQ[i][4]
            boundaryElementsQ[boundaryElementNumber][3] = elementNumbersQ[i][1]
            boundaryElementsQ[boundaryElementNumber][4] = patch_y0
            boundaryElementNumber += 1
        # y = y0 + ly
        if ((abs(ny2-(y0+ly)) < tol) and (abs(ny3-(y0+ly)) < tol)):
            boundaryElementsL[boundaryElementNumber][0] = i + 1
            boundaryElementsL[boundaryElementNumber][1] = elementNumbersL[i][2]
            boundaryElementsL[boundaryElementNumber][2] = elementNumbersL[i][3]
            boundaryElementsL[boundaryElementNumber][3] = patch_y1
            boundaryElementsQ[boundaryElementNumber][0] = i + 1
            boundaryElementsQ[boundaryElementNumber][1] = elementNumbersQ[i][2]
            boundaryElementsQ[boundaryElementNumber][2] = elementNumbersQ[i][8]
            boundaryElementsQ[boundaryElementNumber][3] = elementNumbersQ[i][3]
            boundaryElementsQ[boundaryElementNumber][4] = patch_y1
            boundaryElementNumber += 1
        # x = x0
        if ((abs(nx0-x0) < tol) and (abs(nx2-x0) < tol)):
            boundaryElementsL[boundaryElementNumber][0] = i + 1
            boundaryElementsL[boundaryElementNumber][1] = elementNumbersL[i][0]
            boundaryElementsL[boundaryElementNumber][2] = elementNumbersL[i][2]
            boundaryElementsL[boundaryElementNumber][3] = patch_x0
            boundaryElementsQ[boundaryElementNumber][0] = i + 1
            boundaryElementsQ[boundaryElementNumber][1] = elementNumbersQ[i][0]
            boundaryElementsQ[boundaryElementNumber][2] = elementNumbersQ[i][5]
            boundaryElementsQ[boundaryElementNumber][3] = elementNumbersQ[i][2]
            boundaryElementsQ[boundaryElementNumber][4] = patch_x0
            boundaryElementNumber += 1
        # x = x0 + lx
        if ((abs(nx1-(x0+lx)) < tol) and (abs(nx3-(x0+lx)) < tol)):
            boundaryElementsL[boundaryElementNumber][0] = i + 1
            boundaryElementsL[boundaryElementNumber][1] = elementNumbersL[i][1]
            boundaryElementsL[boundaryElementNumber][2] = elementNumbersL[i][3]
            boundaryElementsL[boundaryElementNumber][3] = patch_x1
            boundaryElementsQ[boundaryElementNumber][0] = i + 1
            boundaryElementsQ[boundaryElementNumber][1] = elementNumbersQ[i][1]
            boundaryElementsQ[boundaryElementNumber][2] = elementNumbersQ[i][7]
            boundaryElementsQ[boundaryElementNumber][3] = elementNumbersQ[i][3]
            boundaryElementsQ[boundaryElementNumber][4] = patch_x1
            boundaryElementNumber += 1
    if (numberOfBoundaryPatches != boundaryElementNumber):
        print ">>>ERROR: Invalid number of boundary patches."
        return
    print " "
    print "compute initial SolVel"
    # drive-lin-elasticity.cpp
    def computeInitSolVelX(x, y):
        s = -0.1 / 64.0 * x * x
        return s
    def computeInitSolVelY(x, y):
        s = 0.1 / 64.0 * x * x * (8.0 - x)
        return s
    # analytical 2D Laplace solution
    def analyticalSolution(x, y):
        s = 2.0 * np.exp(x) * np.cos(y)
        return s
    for i in range(0, numberOfNodesQ, 1):
        initVel[i][0] = computeInitSolVelX(coordinatesq[i][0], coordinatesq[i][1])
        initVel[i][1] = computeInitSolVelY(coordinatesq[i][0], coordinatesq[i][1])
    analyticalL = np.zeros((numberOfNodesL,1), dtype=float)
    analyticalQ = np.zeros((numberOfNodesQ,1), dtype=float)
    for i in range(0, numberOfNodesL, 1):
        analyticalL[i]  = analyticalSolution(coordinatesl[i][0], coordinatesl[i][1])
    print " "
    for i in range(0, numberOfNodesQ, 1):
        analyticalQ[i]  = analyticalSolution(coordinatesq[i][0], coordinatesq[i][1])
    print " "
    print "export X (lin)"
    myheader = " "+str(numberOfNodesL)+" 2"
    np.savetxt(fnameXl, coordinatesl, fmt="%f", header=myheader, comments="")
    print "export X (quad)"
    myheader = " "+str(numberOfNodesQ)+" 2"
    np.savetxt(fnameXq, coordinatesq, fmt="%f", header=myheader, comments="")
    print "export T (lin)"
    myheader = " "+str(numberOfElements)+" "+str(numberOfNodesL)
    np.savetxt(fnameTl, elementNumbersL, fmt="%d", header=myheader, comments="")
    print "export T (quad)"
    myheader = " "+str(numberOfElements)+" "+str(numberOfNodesQ)
    np.savetxt(fnameTq, elementNumbersQ, fmt="%d", header=myheader, comments="")
    print "export B (lin)"
    myheader = " "+str(numberOfBoundaryPatches)+" "+str(numberOfNodesL)
    np.savetxt(fnameBl, boundaryElementsL, fmt="%d", header=myheader, comments="")
    print "export B (quad)"
    myheader = " "+str(numberOfBoundaryPatches)+" "+str(numberOfNodesQ)
    np.savetxt(fnameBq, boundaryElementsQ, fmt="%d", header=myheader, comments="")
    print "export "+fnameInitSpace
    myheader = " "+str(numberOfNodesQ)+" 2"
    np.savetxt(fnameInitSpace, coordinatesq, fmt="%f", header=myheader, comments="")
    print "export "+fnameInitVel
    myheader = " "+str(numberOfNodesQ)+" 2"
    np.savetxt(fnameInitVel, initVel, fmt="%f", header=myheader, comments="")
    print "export "+fnameAnalyticalL
    myheader = " "+str(numberOfNodesL)+" 1"
    np.savetxt(fnameAnalyticalL, analyticalL, fmt="%.16f", header=myheader, comments="")
    print "export "+fnameAnalyticalQ
    myheader = " "+str(numberOfNodesQ)+" 1"
    np.savetxt(fnameAnalyticalQ, analyticalQ, fmt="%.16f", header=myheader, comments="")
#    print " "
#    print "show mesh"
#    fig, ax = plt.subplots()
#    ax.scatter(xq_notsorted, yq_notsorted)
#    for i, txt in enumerate(nodeNumbersQ.flatten()):
#        ax.annotate(txt, (xq_notsorted[i], yq_notsorted[i]))
#    plt.show()

if __name__ == "__main__":
    main()
