from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize("misc/cubeC.pyx",language="c",compiler_directives={'profile': False, 'boundscheck' : False, 'wraparound': False, 'cdivision' : False}))
