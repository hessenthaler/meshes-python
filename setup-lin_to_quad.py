from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize("misc/lin_to_quad.pyx",language="c",compiler_directives={'profile': False}))
