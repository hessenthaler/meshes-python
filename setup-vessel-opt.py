from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize("misc/vesselC.pyx",language="c",compiler_directives={'profile': False, 'boundscheck' : False, 'wraparound': False, 'cdivision' : False}))
