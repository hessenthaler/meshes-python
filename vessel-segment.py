#!/bin/python
################################################################################
# FSI MESH GENERATOR FOR VESSEL SEGMENT
################################################################################
# export format: X/T/B files (CHeart)
################################################################################
#
# geometry:     --------------------------
#               ||||||||||||||||||||||||||
#               --------------------------
#               |                        |
#               + origin                 |      .       .
#               |                        |      |  ri   |
#               --------------------------      v       |  ro
#               ||||||||||||||||||||||||||              |
#               --------------------------              v
#
#               <---------- lz ---------->
#
#
# coordinate system:      y
#                         |  x
#                         | /
#                         |/__ z
#
################################################################################
#
# testing examples for FSIViewer:
#
#   python3 vessel.py domainS 32 5 4 domainF 1 1
#   python3 vessel.py domainS  4 2 2 domainF 1 1
#
################################################################################
#
# HEX mesh for vessel wall
#
# ### 8-node hexahedron, node numbers:
#
#                 bottom    top
#
#                 4---3    8---7
#                 |   |    |   |
#                 2---1    6---5
#
# ### 27-node hexahedron, node numbers:
#
#                  bottom                       top
#
#                 4--13--3     22--21--20     8--27--7
#                 |      |      |      |      |      |
#                12  11  10    19  18  17    26  25  24
#                 |      |      |      |      |      |
#                 2---9--1     16--15--14     6--23--5
#
################################################################################
#
# TET mesh for fluid domain
#
### 4-node/10-node tetrahedron, node numbers:
#
#                            y
#                           /
#                          /
#                         3                       10
#                       /   \                    / \
#                     6      7                 /    \          4
#                    /        \              /       \
#                  1-----5-----2---> x      8_________9
#
# how we compute the mesh: 1 - compute all nodes for a linear topology
#                          2 - compute Delaunay triangulation on a single slice
#                          3 - create a prism mesh
#                          4 - tesselate prism mesh into tet mesh
#                              4.1 solve graph problem for consistent face split
#                              4.2 split prisms into tets (1 prism --> 3 tets)
#                          5 - compute node positions for quadratic topology
#
################################################################################
#
# TRI mesh for LM domain
#
### 6-node triangle, node numbers:
#
#                            y
#                           /
#                          /
#                         3
#                       /   \
#                     5      6
#                    /        \
#                  1-----4-----2---> x
#
################################################################################
#
# Required packages:    python3, python3-dev    >= 3.4
#                       numpy                   >= 1.11.2
#                       scipy                   >= 0.13
#                       cython3                 >= 0.23.4
#
################################################################################
# author: Andreas Hessenthaler
#
# no redistribution, for personal use only
################################################################################

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys
from mpl_toolkits.mplot3d import Axes3D
from scipy import version as scipy_version
from scipy.spatial import Delaunay, delaunay_plot_2d
import logging
import os
sys.path.append("lib/")
import vesselC

def main():

    # logging.basicConfig(level=logging.DEBUG)
    logging.basicConfig(level=logging.INFO)

    scipyVersion = scipy_version.version
    scipyVersionMajor = int(scipyVersion[0])
    scipyVersionMinor = int(scipyVersion[2:4:1])
    if ((scipyVersionMajor == 0) and (scipyVersionMinor <= 13)):
        print(">>>ERROR: Requires scipy version newer than 0.13. You have " \
            +str(scipyVersionMajor)+"."+str(scipyVersionMinor)+"!")
        sys.exit()

    if (len(sys.argv) != 9):
        print("missing or invalid commandline arguments:")
        print("")
        print("    python3 vessel-segment.py param1 param2 param3 param4 param5 param6 param7 param8")
        print("")
        print("                     param1 - prefix for solid (e.g. \'domainS\')")
        print("                     param2 - number of subdivisions in circumferential direction")
        print("                     param3 - number of subdivisions in radial direction")
        print("                     param4 - number of subdivisions in axial direction")
        print("                     param5 - prefix for fluid (e.g. \'domainF\')")
        print("                     param6 - number of subdivisions in circumferential direction equals (param2 * param6)")
        print("                            - number of subdivisions in radial direction is adjusted to derive an approximately uniform 2D mesh in z-slices")
        print("                     param7 - number of subdivisions in axial direction equals (param4 * param7)")
        print("                     param8 - prefix for LM (e.g. \'lm\')")
        print("")
        return

    fileprefixS  = str(sys.argv[1])
    fileprefixF  = str(sys.argv[5])
    fileprefixLM = str(sys.argv[8])
    ### subdivisions for solid
    # number of subdivisions to approximate circular cross-section
    sc = int(sys.argv[2])
    # number of elements subdividing wall thickness
    st = int(sys.argv[3])
    # number of elements in axial direction
    sl = int(sys.argv[4])

    #### tesselate fluid
    fc = int(sys.argv[6])
    fl = int(sys.argv[7])

    tol = 1.0e-6

    # inner radius, outer radius, length of cylinder
    ri =  0.7
    ro =  1.0
    lz =  1.0

    # origin position
    x0 = 0.0
    y0 = 0.0
    z0 = 0.0

    # BC patches
    patchS_z0   = 1
    patchS_lz   = 2
    patchS_ri   = 3
    patchS_ro   = 4
    patchS_x0   = 5
    patchS_y0   = 6
    patchF_z0   = 1
    patchF_lz   = 2
    patchF_ri   = 3
    patchF_x0   = 4
    patchF_y0   = 5
    patchLM_z0  = 1
    patchLM_lz  = 2
    patchLM_x0  = 3
    patchLM_y0  = 4
    patchDir  = "data"

    # move quad-nodes to ri (fluid)
    morphFSboundary = True

    # solve graph problem to split prisms to tets?
    # if true : results in three tets
    # if false: results in 14 tets
    solveGraphProblem = False

    # perform sanity checks
    err = 0
    if (sc < 2):
        print(">>>ERROR: A circle cannot be approximated by a single point!")
        err = 1
    if ((ri <= 0.0) or (ro <= 0.0) or (ri >= ro)):
        print(">>>ERROR: 0 < r_i < r_o is not satisfied.")
        print("          Check values for inner radius and outer radius!")
        err = 1
    if (err == 1):
        return
    if (not(os.path.exists(patchDir))):
        print(">>>WARNING: Folder "+patchDir+"/ did not exist and was created.")
        os.makedirs(patchDir)

    # allocate memory to store points of solid mesh (linear, hexahedra)
    numberOfNodesLinS  = (sc + 1) * (st + 1) * (sl + 1)
    numberOfNodesQuadS = (2 * sc + 1) * (2 * st + 1) * (2 * sl + 1)
    numberOfElementsS  = sc * st * sl

    print("!==================================================================!")
    print("Creating hex mesh for solid wall...")
    print("    Number of nodes (lin):  "+str(numberOfNodesLinS))
    print("    Number of nodes (quad): "+str(numberOfNodesQuadS))

    ### nodes subdividing circles with radius ri <= r <= ro at x > x0 ##########
    xLinS         = np.zeros((numberOfNodesLinS), dtype=float)
    yLinS         = np.zeros((numberOfNodesLinS), dtype=float)
    zLinS         = np.zeros((numberOfNodesLinS), dtype=float)
    xQuadS        = np.zeros((numberOfNodesQuadS), dtype=float)
    yQuadS        = np.zeros((numberOfNodesQuadS), dtype=float)
    zQuadS        = np.zeros((numberOfNodesQuadS), dtype=float)
    nodeListLinS  = range(1, numberOfNodesLinS+1,  1)
    nodeListQuadS = range(1, numberOfNodesQuadS+1, 1)
    patchesLinS   = np.zeros((numberOfNodesLinS), dtype=int)
    patchesQuadS  = np.zeros((numberOfNodesQuadS), dtype=int)
    ## firstly, compute node coordinates of those nodes
    ## that exist on both lin/quad meshes
    # loop for dz increment
    offset = 0
    for j in range(0, sl+1, 1):
        for i in range(0, st+1, 1):
            r = (ri + (ro - ri) / float(st) * float(i))
            # x
            xLinS[offset:offset+sc+1:1] = \
                r * np.cos(np.linspace(0.0, float(sc+1), sc+1, endpoint=True) * 0.5 * np.pi / float(sc+1))
            xQuadS[offset:offset+sc+1:1] = \
                r * np.cos(np.linspace(0.0, float(sc+1), sc+1, endpoint=True) * 0.5 * np.pi / float(sc+1))
            # y
            yLinS[offset:offset+sc+1:1] = \
                r * np.sin(np.linspace(0.0, float(sc+1), sc+1, endpoint=True) * 0.5 * np.pi / float(sc+1))
            yQuadS[offset:offset+sc+1:1] = \
                r * np.sin(np.linspace(0.0, float(sc+1), sc+1, endpoint=True) * 0.5 * np.pi / float(sc+1))
            # z
            zLinS[offset:offset+sc+1:1] = \
                float(j) * (lz / float(sl))
            zQuadS[offset:offset+sc+1:1] = \
                float(j) * (lz / float(sl))
            offset += sc + 1
    offsetSave = offset
    # print(xQuadS[0:offsetSave:1])
    # print(yQuadS[0:offsetSave:1])
    # print(zQuadS[0:offsetSave:1])
    # return
    ## secondly, compute node coordinates that only exist on quad mesh
    for j in range(0, 2*sl+1, 1):
        if (np.mod(j, 2) == 0):
            for i in range(0, 2*st+1, 1):
                r = (ri + (ro - ri) / float(2*st) * float(i))
                if (np.mod(i, 2) == 0):
                    # x
                    xQuadS[offset:offset+sc:1] = \
                        r * np.cos(np.linspace(0.0, float(sc), sc, endpoint=False) * 0.5 * np.pi / float(sc) + 0.25 * np.pi / float(sc))
                    # y
                    yQuadS[offset:offset+sc:1] = \
                        r * np.sin(np.linspace(0.0, float(sc), sc, endpoint=False) * 0.5 * np.pi / float(sc) + 0.25 * np.pi / float(sc))
                    # z
                    zQuadS[offset:offset+sc:1] = \
                        float(j) * (lz / float(2*sl))
                    offset += sc
                else:
                    # x
                    xQuadS[offset:offset+2*sc+1:1] = \
                        r * np.cos(np.linspace(0.0, float(2*sc+1), 2*sc+1, endpoint=True) * 0.5 * np.pi / float(2*sc+1))
                    # y
                    yQuadS[offset:offset+2*sc+1:1] = \
                        r * np.sin(np.linspace(0.0, float(2*sc+1), 2*sc+1, endpoint=True) * 0.5 * np.pi / float(2*sc+1))
                    # z
                    zQuadS[offset:offset+2*sc+1:1] = \
                        float(j) * (lz / float(2*sl))
                    offset += 2*sc + 1
            # print(xQuadS[offsetSave:offset:1])
            # print(yQuadS[offsetSave:offset:1])
            # print(zQuadS[offsetSave:offset:1])
            # return
            offsetSave = offset
        else:
            # those nodes that corresponds to nodes at half-way points of element edges in the radial direction
            for i in range(0, 2*st+1, 1):
                r = (ri + (ro - ri) / float(2*st) * float(i))
                # x
                xQuadS[offset:offset+2*sc+1:1] = \
                    r * np.cos(np.linspace(0.0, float(2*sc+1), 2*sc+1, endpoint=True) * 0.5 * np.pi / float(2*sc+1))
                # y
                yQuadS[offset:offset+2*sc+1:1] = \
                    r * np.sin(np.linspace(0.0, float(2*sc+1), 2*sc+1, endpoint=True) * 0.5 * np.pi / float(2*sc+1))
                # z
                zQuadS[offset:offset+2*sc+1:1] = \
                    float(j) * (lz / float(2*sl))
                offset += 2*sc + 1
            # print(xQuadS[offsetSave:offset:1])
            # print(yQuadS[offsetSave:offset:1])
            # print(zQuadS[offsetSave:offset:1])
            # return
    # print(xQuadS)
    # print(yQuadS)
    # print(zQuadS)
    # return

    # move origin if necessary
    if (abs(x0) > tol):
        xLinS  += x0
        xQuadS += x0
    if (abs(y0) > tol):
        yLinS  += y0
        yQuadS += y0
    if (abs(z0) > tol):
        zLinS  += z0
        zQuadS += z0
    # reshape for easy export
    pointsLinS  = np.column_stack((xLinS, yLinS, zLinS))
    pointsQuadS = np.column_stack((xQuadS, yQuadS, zQuadS))
    if (vesselC.sanityCheckHexahedra(xLinS, yLinS, zLinS, xQuadS, yQuadS, zQuadS, tol) == -1):
        return

    ### compute mesh connectivity ##############################################
    # we assume that entry [i][0:8:1]  contains node list for linear    elements
    #                entry [i][0:27:1] contains node list for quadratic elements
    elementNodeListS = np.zeros((numberOfElementsS, 27), dtype=int)

    numberOfBoundaryElementsS = 2 * (sc * st + sc * sl + st * sl)
    boundaryPatchesLinS       = np.zeros((numberOfBoundaryElementsS,  6), dtype=int)
    boundaryPatchesQuadS      = np.zeros((numberOfBoundaryElementsS, 11), dtype=int)

    # change node list order here if different convention is used
    numberOfNodesInQuadSliceS         = (2 * st + 1) * (2 * sc + 1)
    numberOfQuadOnlyNodesInLinSliceS  = (st + 1) * sc + st * (2 * sc + 1) # this is the number of quad-only nodes
    firstElementNodeList              = np.zeros((27), dtype=int)
    # element corners on bottom plane
    firstElementNodeList[ 0] = 2
    firstElementNodeList[ 1] = 1
    firstElementNodeList[ 2] = 2+(sc+1)
    firstElementNodeList[ 3] = 1+(sc+1)
    # element corners on top plane
    firstElementNodeList[ 4] = 2+(sc+1)*(st+1)
    firstElementNodeList[ 5] = 1+(sc+1)*(st+1)
    firstElementNodeList[ 6] = 2+(sc+1)*(st+2)
    firstElementNodeList[ 7] = 1+(sc+1)*(st+2)
    # quad-only nodes on bottom plane
    firstElementNodeList[ 8] = numberOfNodesLinS+1
    firstElementNodeList[ 9] = numberOfNodesLinS+1 + sc+2
    firstElementNodeList[10] = numberOfNodesLinS+1 + sc+1
    firstElementNodeList[11] = numberOfNodesLinS+1 + sc+0
    firstElementNodeList[12] = numberOfNodesLinS+1 + sc+(2*sc+1)
    #
    firstElementNodeList[13] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3
    firstElementNodeList[14] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+2
    firstElementNodeList[15] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+1
    firstElementNodeList[16] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3 +   2*(sc+1)-1
    firstElementNodeList[17] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3 +   2*(sc+1)-2
    firstElementNodeList[18] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3 +   2*(sc+1)-3
    firstElementNodeList[19] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3 + 2*2*(sc+1)-2
    firstElementNodeList[20] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3 + 2*2*(sc+1)-3
    firstElementNodeList[21] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS+3 + 2*2*(sc+1)-4
    firstElementNodeList[22] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS + numberOfNodesInQuadSliceS + 1
    firstElementNodeList[23] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS + numberOfNodesInQuadSliceS + 1 + sc + 2
    firstElementNodeList[24] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS + numberOfNodesInQuadSliceS + 1 + sc + 1
    firstElementNodeList[25] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS + numberOfNodesInQuadSliceS + 1 + sc
    firstElementNodeList[26] = numberOfNodesLinS + numberOfQuadOnlyNodesInLinSliceS + numberOfNodesInQuadSliceS + 1 + sc     + 2*(sc+1)-1
    # print(firstElementNodeList)
    # return

    # for each element, assign node list (8-node hexahedra)
    # based on a template defined above
    elementNumber         = 0
    boundaryElementNumber = 0
    for k in range(0, sl, 1):
        for j in range(0, st, 1):
            for i in range(0, sc, 1):
                elementNumber = i+j*sc+k*sc*st
                logging.debug("Element "+str(elementNumber)+"(i "+str(i)+", j "+str(j)+"):")
                elementNodeListS[elementNumber][0:8:1] = \
                    firstElementNodeList[0:8:1] + i+j*(sc+1)+k*(sc+1)*(st+1)
                elementNodeListS[elementNumber][8:27:1] = \
                    firstElementNodeList[8:27:1] + i+j*(2*sc+sc+1)+k*((sc)*(st+1)+(2*sc+1)*st+(2*sc+1)*(2*st+1))
                elementNodeListS[elementNumber][ 9] += i
                elementNodeListS[elementNumber][10] += i
                elementNodeListS[elementNumber][11] += i
                elementNodeListS[elementNumber][13] += i+(sc+1)*j
                elementNodeListS[elementNumber][14] += i+(sc+1)*j
                elementNodeListS[elementNumber][15] += i+(sc+1)*j
                elementNodeListS[elementNumber][16] += i+(sc+1)*j
                elementNodeListS[elementNumber][17] += i+(sc+1)*j
                elementNodeListS[elementNumber][18] += i+(sc+1)*j
                elementNodeListS[elementNumber][19] += i+(sc+1)*j
                elementNodeListS[elementNumber][20] += i+(sc+1)*j
                elementNodeListS[elementNumber][21] += i+(sc+1)*j
                elementNodeListS[elementNumber][23] += i
                elementNodeListS[elementNumber][24] += i
                elementNodeListS[elementNumber][25] += i
                # elementNodeListS[elementNumber][26] -= 1
                # print(elementNodeListS[elementNumber][0:8:1])
                # print(elementNodeListS[elementNumber][8:9:1])
                # print(elementNodeListS[elementNumber][9:12:1])
                # print(elementNodeListS[elementNumber][12:13:1])
                # print(elementNodeListS[elementNumber][13:16:1])
                # print(elementNodeListS[elementNumber][16:19:1])
                # print(elementNodeListS[elementNumber][19:22:1])
                # print(elementNodeListS[elementNumber][22:23:1])
                # print(elementNodeListS[elementNumber][23:26:1])
                # print(elementNodeListS[elementNumber][26:27:1])
                # print(elementNodeListS[elementNumber][:])
                # return
                # z = z0
                if (k == 0):
                    # lin-hex
                    boundaryPatchesLinS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesLinS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][0]
                    boundaryPatchesLinS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][1]
                    boundaryPatchesLinS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][2]
                    boundaryPatchesLinS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][3]
                    boundaryPatchesLinS[boundaryElementNumber][5] = \
                      patchS_z0
                    # quad-hex
                    boundaryPatchesQuadS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesQuadS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][0]
                    boundaryPatchesQuadS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][1]
                    boundaryPatchesQuadS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][2]
                    boundaryPatchesQuadS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][3]
                    boundaryPatchesQuadS[boundaryElementNumber][5] = \
                      elementNodeListS[elementNumber][8]
                    boundaryPatchesQuadS[boundaryElementNumber][6] = \
                      elementNodeListS[elementNumber][9]
                    boundaryPatchesQuadS[boundaryElementNumber][7] = \
                      elementNodeListS[elementNumber][10]
                    boundaryPatchesQuadS[boundaryElementNumber][8] = \
                      elementNodeListS[elementNumber][11]
                    boundaryPatchesQuadS[boundaryElementNumber][9] = \
                      elementNodeListS[elementNumber][12]
                    boundaryPatchesQuadS[boundaryElementNumber][10] = \
                      patchS_z0
                    boundaryElementNumber += 1
                # z = lz
                if (k == sl-1):
                    # lin-hex
                    boundaryPatchesLinS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesLinS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][4]
                    boundaryPatchesLinS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][5]
                    boundaryPatchesLinS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][6]
                    boundaryPatchesLinS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][7]
                    boundaryPatchesLinS[boundaryElementNumber][5] = \
                      patchS_lz
                    # quad-hex
                    boundaryPatchesQuadS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesQuadS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][4]
                    boundaryPatchesQuadS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][5]
                    boundaryPatchesQuadS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][6]
                    boundaryPatchesQuadS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][7]
                    boundaryPatchesQuadS[boundaryElementNumber][5] = \
                      elementNodeListS[elementNumber][22]
                    boundaryPatchesQuadS[boundaryElementNumber][6] = \
                      elementNodeListS[elementNumber][23]
                    boundaryPatchesQuadS[boundaryElementNumber][7] = \
                      elementNodeListS[elementNumber][24]
                    boundaryPatchesQuadS[boundaryElementNumber][8] = \
                      elementNodeListS[elementNumber][25]
                    boundaryPatchesQuadS[boundaryElementNumber][9] = \
                      elementNodeListS[elementNumber][26]
                    boundaryPatchesQuadS[boundaryElementNumber][10] = \
                      patchS_lz
                    boundaryElementNumber += 1
                # r = ri
                if (j == 0):
                    # lin-hex
                    boundaryPatchesLinS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesLinS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][0]
                    boundaryPatchesLinS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][1]
                    boundaryPatchesLinS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][4]
                    boundaryPatchesLinS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][5]
                    boundaryPatchesLinS[boundaryElementNumber][5] = \
                      patchS_ri
                    # quad-hex
                    boundaryPatchesQuadS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesQuadS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][0]
                    boundaryPatchesQuadS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][1]
                    boundaryPatchesQuadS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][4]
                    boundaryPatchesQuadS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][5]
                    boundaryPatchesQuadS[boundaryElementNumber][5] = \
                      elementNodeListS[elementNumber][8]
                    boundaryPatchesQuadS[boundaryElementNumber][6] = \
                      elementNodeListS[elementNumber][13]
                    boundaryPatchesQuadS[boundaryElementNumber][7] = \
                      elementNodeListS[elementNumber][14]
                    boundaryPatchesQuadS[boundaryElementNumber][8] = \
                      elementNodeListS[elementNumber][15]
                    boundaryPatchesQuadS[boundaryElementNumber][9] = \
                      elementNodeListS[elementNumber][22]
                    boundaryPatchesQuadS[boundaryElementNumber][10] = \
                      patchS_ri
                    boundaryElementNumber += 1
                # r = ro
                if (j == st-1):
                    # lin-hex
                    boundaryPatchesLinS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesLinS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][2]
                    boundaryPatchesLinS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][3]
                    boundaryPatchesLinS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][6]
                    boundaryPatchesLinS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][7]
                    boundaryPatchesLinS[boundaryElementNumber][5] = \
                      patchS_ro
                    # quad-hex
                    boundaryPatchesQuadS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesQuadS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][2]
                    boundaryPatchesQuadS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][3]
                    boundaryPatchesQuadS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][6]
                    boundaryPatchesQuadS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][7]
                    boundaryPatchesQuadS[boundaryElementNumber][5] = \
                      elementNodeListS[elementNumber][12]
                    boundaryPatchesQuadS[boundaryElementNumber][6] = \
                      elementNodeListS[elementNumber][19]
                    boundaryPatchesQuadS[boundaryElementNumber][7] = \
                      elementNodeListS[elementNumber][20]
                    boundaryPatchesQuadS[boundaryElementNumber][8] = \
                      elementNodeListS[elementNumber][21]
                    boundaryPatchesQuadS[boundaryElementNumber][9] = \
                      elementNodeListS[elementNumber][26]
                    boundaryPatchesQuadS[boundaryElementNumber][10] = \
                      patchS_ro
                    boundaryElementNumber += 1
                # y = 0
                if (i == 0):
                    # lin-hex
                    boundaryPatchesLinS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesLinS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][1]
                    boundaryPatchesLinS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][3]
                    boundaryPatchesLinS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][5]
                    boundaryPatchesLinS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][7]
                    boundaryPatchesLinS[boundaryElementNumber][5] = \
                      patchS_y0
                    # quad-hex
                    boundaryPatchesQuadS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesQuadS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][1]
                    boundaryPatchesQuadS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][3]
                    boundaryPatchesQuadS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][5]
                    boundaryPatchesQuadS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][7]
                    boundaryPatchesQuadS[boundaryElementNumber][5] = \
                      elementNodeListS[elementNumber][11]
                    boundaryPatchesQuadS[boundaryElementNumber][6] = \
                      elementNodeListS[elementNumber][15]
                    boundaryPatchesQuadS[boundaryElementNumber][7] = \
                      elementNodeListS[elementNumber][18]
                    boundaryPatchesQuadS[boundaryElementNumber][8] = \
                      elementNodeListS[elementNumber][21]
                    boundaryPatchesQuadS[boundaryElementNumber][9] = \
                      elementNodeListS[elementNumber][25]
                    boundaryPatchesQuadS[boundaryElementNumber][10] = \
                      patchS_y0
                    boundaryElementNumber += 1
                # x = 0
                if (i == sc-1):
                    # lin-hex
                    boundaryPatchesLinS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesLinS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][0]
                    boundaryPatchesLinS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][2]
                    boundaryPatchesLinS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][4]
                    boundaryPatchesLinS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][6]
                    boundaryPatchesLinS[boundaryElementNumber][5] = \
                      patchS_x0
                    # quad-hex
                    boundaryPatchesQuadS[boundaryElementNumber][0] = \
                      elementNumber + 1 # zero-based
                    boundaryPatchesQuadS[boundaryElementNumber][1] = \
                      elementNodeListS[elementNumber][0]
                    boundaryPatchesQuadS[boundaryElementNumber][2] = \
                      elementNodeListS[elementNumber][2]
                    boundaryPatchesQuadS[boundaryElementNumber][3] = \
                      elementNodeListS[elementNumber][4]
                    boundaryPatchesQuadS[boundaryElementNumber][4] = \
                      elementNodeListS[elementNumber][6]
                    boundaryPatchesQuadS[boundaryElementNumber][5] = \
                      elementNodeListS[elementNumber][9]
                    boundaryPatchesQuadS[boundaryElementNumber][6] = \
                      elementNodeListS[elementNumber][13]
                    boundaryPatchesQuadS[boundaryElementNumber][7] = \
                      elementNodeListS[elementNumber][16]
                    boundaryPatchesQuadS[boundaryElementNumber][8] = \
                      elementNodeListS[elementNumber][19]
                    boundaryPatchesQuadS[boundaryElementNumber][9] = \
                      elementNodeListS[elementNumber][23]
                    boundaryPatchesQuadS[boundaryElementNumber][10] = \
                      patchS_x0
                    boundaryElementNumber += 1

    if (boundaryElementNumber != numberOfBoundaryElementsS):
        print(">>>ERROR: Invalid number of boundary elements!")
        return

    # store boundary surface separately
    patchS = np.zeros((numberOfNodesLinS, 1), dtype=int)
    for i in range(0, numberOfBoundaryElementsS, 1):
        for j in range(1, 5, 1):
            currentNodeNumber = boundaryPatchesLinS[i][j] - 1 # need zero-based index
            if (patchS[currentNodeNumber] != patchS_ri):
                patchS[currentNodeNumber] = boundaryPatchesLinS[i][5]
            if (patchS[currentNodeNumber] != patchS_ro):
                patchS[currentNodeNumber] = boundaryPatchesLinS[i][5]
            if (patchS[currentNodeNumber] != patchS_x0):
                patchS[currentNodeNumber] = boundaryPatchesLinS[i][5]
            if (patchS[currentNodeNumber] != patchS_y0):
                patchS[currentNodeNumber] = boundaryPatchesLinS[i][5]

    print("    Number of elements:     "+str(numberOfElementsS))
    print("!==================================================================!")
    print("Creating tet mesh for fluid...")

    # tesselate fluid-solid surface on fluid-side
    sc *= fc
    sl *= fl

    # compute circumference and subdivision to find an approximate uniform
    # mesh on a quarter of a 2D disk
    circumference = 0.5 * np.pi * ri
    circumference_edgeLength = circumference / (sc + 1)
    numberOfCircles = int(np.ceil(ri / circumference_edgeLength))
    dr = ri / numberOfCircles
    print("    Number of circles (lin):  "+str(numberOfCircles))

    # figure out how many nodes we have on a disk
    disk_numberOfNodesLinF  = 1 # count the origin as well!
    for i in range(1, numberOfCircles+1, 1):
        r = float(i) * dr
        circumference = 0.5 * np.pi * r
        circumference_numberOfNodes = int(np.ceil(circumference / circumference_edgeLength))
        disk_numberOfNodesLinF  += circumference_numberOfNodes

    numberOfNodesLinF = int((sl + 1) * disk_numberOfNodesLinF)
    pointsLinF        = np.zeros((numberOfNodesLinF*3), dtype=float)

    # create fluid nodes
    offset = 0
    for j in range(0, sl+1, 1):
        for i in range(0, numberOfCircles+1, 1):
            r = float(i) * dr
            circumference = 0.5 * np.pi * r
            if (i == 0):
                circumference_numberOfNodes = 1
            else:
                circumference_numberOfNodes = \
                    int(np.ceil(circumference / circumference_edgeLength))
            pointsLinF[offset:offset+circumference_numberOfNodes:1] = \
                r * np.cos(np.linspace( \
                0.0, float(circumference_numberOfNodes), circumference_numberOfNodes, endpoint=True) \
                * 0.5 * np.pi / circumference_numberOfNodes)
            pointsLinF[numberOfNodesLinF+offset:numberOfNodesLinF+offset+circumference_numberOfNodes:1] = \
                r * np.sin(np.linspace( \
                0.0, float(circumference_numberOfNodes), circumference_numberOfNodes, endpoint=True) \
                * 0.5 * np.pi / circumference_numberOfNodes)
            pointsLinF[numberOfNodesLinF*2+offset:numberOfNodesLinF*2+offset+circumference_numberOfNodes:1] = \
                j * lz / sl
            offset += circumference_numberOfNodes

    # get nodes for z = z0
    x = pointsLinF[0*numberOfNodesLinF:0*numberOfNodesLinF+disk_numberOfNodesLinF:1]
    y = pointsLinF[1*numberOfNodesLinF:1*numberOfNodesLinF+disk_numberOfNodesLinF:1]
    z = pointsLinF[2*numberOfNodesLinF:2*numberOfNodesLinF+disk_numberOfNodesLinF:1]

    # compute Delaunay triangulation
    points = np.column_stack((x, y))
    tri = Delaunay(points)
    numberOfTrianglesLinF = tri.simplices.shape[0]

    if True:
        # make sure first node number is smallest
        for i in range(0, numberOfTrianglesLinF, 1):
            nn0 = tri.simplices[i][0]
            nn1 = tri.simplices[i][1]
            nn2 = tri.simplices[i][2]
            if ((nn2 < nn1) and (nn1 < nn0)):
                tri.simplices[i][0] = nn2
                tri.simplices[i][1] = nn0
                tri.simplices[i][2] = nn1
            elif (nn1 < nn0):
                tri.simplices[i][0] = nn1
                tri.simplices[i][1] = nn2
                tri.simplices[i][2] = nn0
            elif (nn2 < nn0):
                tri.simplices[i][0] = nn2
                tri.simplices[i][1] = nn0
                tri.simplices[i][2] = nn1
            logging.debug(tri.simplices[i][0:3:1])
        # sanity check
        for i in range(0, numberOfTrianglesLinF, 1):
            nn0 = tri.simplices[i][0]
            nn1 = tri.simplices[i][1]
            nn2 = tri.simplices[i][2]
            if (not(nn0 < nn1) and not(nn0 < nn2)):
                print(">>>ERROR: Node order "+str(nn0)+" "+str(nn1)+" "+str(nn2))
                sys.exit()

    # plot Delaunay triangulation
    # fig = plt.figure()
    # plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
    # plt.plot(points[:,0], points[:,1], 'o')
    # for j, s in enumerate(tri.simplices):
    #     p = points[s].mean(axis=0)
    #     plt.text(p[0], p[1], '#%d' % j, ha='center') # label triangles
    # plt.show()
    # return

    # we have two ways to compute a tesselation:
    # 1) a split of each prism into 3 tetrahedra (involves solving a graph problem; expensive)
    # 2) a simple split of each prism into 14 tetrahedra
    if not(solveGraphProblem):
        # make indices 1-based
        disk_trianglesLinF =  tri.simplices+1

        # entering this case:
        # pointsLinF contain all nodes at corners of prisms (numberOfNodesLinF)
        incompleteNumberOfNodesLinF = numberOfNodesLinF
        disk_numberOfTrianglesLinF  = disk_trianglesLinF.shape[0]
        numberOfPrismsF             = sl * disk_numberOfTrianglesLinF
        numberOfTetsF               = 14 * numberOfPrismsF
        minimumMissingNodesLinF     = numberOfTetsF
        # xyzLinF                     = np.copy(pointsLinF.reshape((3, numberOfNodesLinF)).transpose())
        xyzLinF                     = np.concatenate( \
                                          (np.copy(pointsLinF.reshape((3, numberOfNodesLinF)).transpose()), \
                                           np.zeros((minimumMissingNodesLinF, 3)) \
                                          ), \
                                          axis=0)

        # allocate arrays for prisms and tets
        prismsLinF                  = np.zeros((numberOfPrismsF, 6), dtype=int)
        tetsLinF                    = np.zeros((numberOfTetsF,   4), dtype=int)
        tetsQuadF                   = np.zeros((numberOfTetsF,  10), dtype=int)

        # compute mesh connectivity for prisms
        for j in range(0, sl, 1):
            for i in range(0, disk_numberOfTrianglesLinF, 1):
                prismsLinF[i+j*disk_numberOfTrianglesLinF][0:3:1] = \
                    disk_trianglesLinF[i][0:3:1] + disk_numberOfNodesLinF * (j + 0)
                prismsLinF[i+j*disk_numberOfTrianglesLinF][3:6:1] = \
                    disk_trianglesLinF[i][0:3:1] + disk_numberOfNodesLinF * (j + 1)

        # do a pass over all elements to compute new coordinates at center of prism
        # and at centers of quadrilateral faces
        tetID       = 0
        lin2quadTol = lz - z0
        layerOffset = 0
        addedNodes  = 0
        for prismID in range(0, numberOfPrismsF):
            # every time we finish one layer of prisms,
            # we don't have to search all those nodes that are contained in the previous layer..
            # thus, we can set an offset corresponding to the added nodes in the previous layers
            if ((prismID % disk_numberOfTrianglesLinF) == 0):
                  saveLayerOffset = layerOffset
                  layerOffset = numberOfNodesLinF - incompleteNumberOfNodesLinF
                  # let's double-check if we are setting a correct offset
                  if (addedNodes != layerOffset):
                      print(">>>ERROR: Number of added nodes causes an incorrect offset.")
                      return
            # get zero-based node IDs
            n1    = prismsLinF[prismID][0] - 1
            n2    = prismsLinF[prismID][1] - 1
            n3    = prismsLinF[prismID][2] - 1
            n4    = prismsLinF[prismID][3] - 1
            n5    = prismsLinF[prismID][4] - 1
            n6    = prismsLinF[prismID][5] - 1
            # get respective coordinates
            xyz1  = xyzLinF[n1][:].copy()
            xyz2  = xyzLinF[n2][:].copy()
            xyz3  = xyzLinF[n3][:].copy()
            xyz4  = xyzLinF[n4][:].copy()
            xyz5  = xyzLinF[n5][:].copy()
            xyz6  = xyzLinF[n6][:].copy()
            # get new node coordinates
            xyz7  = 0.25 * (xyz1 + xyz2 + xyz4 + xyz5)
            xyz8  = 0.25 * (xyz1 + xyz3 + xyz4 + xyz6)
            xyz9  = 0.25 * (xyz2 + xyz3 + xyz5 + xyz6)
            xyz10 =        (xyz1 + xyz2 + xyz3 + xyz4 + xyz5 + xyz6) / 6.0
            prism2tetTol  = 0.1 * np.linalg.norm(xyz1-xyz2)
            lin2quadTol   = min(lin2quadTol, prism2tetTol)
            # if this is the first prism element, all nodes are new
            if (incompleteNumberOfNodesLinF == numberOfNodesLinF):
                # xyzLinF = np.concatenate((xyzLinF.copy(), np.zeros((4, 3))), axis=0)
                xyzLinF[numberOfNodesLinF+0][:]  = xyz7.copy()
                xyzLinF[numberOfNodesLinF+1][:]  = xyz8.copy()
                xyzLinF[numberOfNodesLinF+2][:]  = xyz9.copy()
                xyzLinF[numberOfNodesLinF+3][:]  = xyz10.copy()
                numberOfNodesLinF += 4
                # assign zero-based node IDs
                n7  = incompleteNumberOfNodesLinF + 0
                n8  = incompleteNumberOfNodesLinF + 1
                n9  = incompleteNumberOfNodesLinF + 2
                n10 = incompleteNumberOfNodesLinF + 3
                addedNodes += 4
            else:
                # check if other nodes already exist
                found = False
                for nodeID in range(incompleteNumberOfNodesLinF+layerOffset, numberOfNodesLinF):
                    xyzCheck  = xyzLinF[nodeID][:].copy()
                    if (np.linalg.norm(xyzCheck-xyz7) < prism2tetTol):
                        n7    = nodeID
                        found = True
                        break
                if not(found):
                    # assign zero-based node IDs
                    n7                              = numberOfNodesLinF
                    numberOfNodesLinF              += 1
                    addedNodes                     += 1
                    # xyzLinF                         = np.concatenate((xyzLinF.copy(), np.zeros((1, 3))), axis=0)
                    xyzLinF[numberOfNodesLinF-1][:] = xyz7.copy()
                found = False
                for nodeID in range(incompleteNumberOfNodesLinF+layerOffset, numberOfNodesLinF):
                    xyzCheck  = xyzLinF[nodeID][:].copy()
                    if (np.linalg.norm(xyzCheck-xyz8) < prism2tetTol):
                        n8    = nodeID
                        found = True
                        break
                if not(found):
                    # assign zero-based node IDs
                    n8                              = numberOfNodesLinF
                    numberOfNodesLinF              += 1
                    addedNodes                     += 1
                    # xyzLinF                         = np.concatenate((xyzLinF.copy(), np.zeros((1, 3))), axis=0)
                    xyzLinF[numberOfNodesLinF-1][:] = xyz8.copy()
                found = False
                for nodeID in range(incompleteNumberOfNodesLinF+layerOffset, numberOfNodesLinF):
                    xyzCheck  = xyzLinF[nodeID][:].copy()
                    if (np.linalg.norm(xyzCheck-xyz9) < prism2tetTol):
                        n9    = nodeID
                        found = True
                        break
                if not(found):
                    # assign zero-based node IDs
                    n9                              = numberOfNodesLinF
                    numberOfNodesLinF              += 1
                    addedNodes                     += 1
                    # xyzLinF                         = np.concatenate((xyzLinF.copy(), np.zeros((1, 3))), axis=0)
                    xyzLinF[numberOfNodesLinF-1][:] = xyz9.copy()
                # the centroid is always new
                # assign zero-based node IDs
                n10                             = numberOfNodesLinF
                numberOfNodesLinF              += 1
                addedNodes                     += 1
                # xyzLinF                         = np.concatenate((xyzLinF.copy(), np.zeros((1, 3))), axis=0)
                xyzLinF[numberOfNodesLinF-1][:] = xyz10.copy()
            # set tetrahedra's 1-based node IDs
            tetsLinF[tetID+ 0][0:4:1] = np.array([n1, n2, n3, n10]) + 1
            tetsLinF[tetID+ 1][0:4:1] = np.array([n4, n6, n5, n10]) + 1
            tetsLinF[tetID+ 2][0:4:1] = np.array([n1, n7, n2, n10]) + 1
            tetsLinF[tetID+ 3][0:4:1] = np.array([n1, n4, n7, n10]) + 1
            tetsLinF[tetID+ 4][0:4:1] = np.array([n7, n5, n2, n10]) + 1
            tetsLinF[tetID+ 5][0:4:1] = np.array([n7, n4, n5, n10]) + 1
            tetsLinF[tetID+ 6][0:4:1] = np.array([n1, n8, n4, n10]) + 1
            tetsLinF[tetID+ 7][0:4:1] = np.array([n8, n3, n6, n10]) + 1
            tetsLinF[tetID+ 8][0:4:1] = np.array([n1, n3, n8, n10]) + 1
            tetsLinF[tetID+ 9][0:4:1] = np.array([n4, n8, n6, n10]) + 1
            tetsLinF[tetID+10][0:4:1] = np.array([n3, n9, n6, n10]) + 1
            tetsLinF[tetID+11][0:4:1] = np.array([n9, n2, n5, n10]) + 1
            tetsLinF[tetID+12][0:4:1] = np.array([n9, n5, n6, n10]) + 1
            tetsLinF[tetID+13][0:4:1] = np.array([n3, n2, n9, n10]) + 1
            tetID += 14


        # double-check whether the tesselation is valid
        if (numberOfTetsF != tetID):
            print(">>>ERROR: Invalid number of tets!")
            return

        # get rid of unused rows
        # print(xyzLinF.shape)
        xyzLinF = np.delete(xyzLinF.copy(), \
                            range(numberOfNodesLinF, incompleteNumberOfNodesLinF+minimumMissingNodesLinF, 1), \
                            axis=0 \
                           )
        # print(numberOfNodesLinF)
        # print(incompleteNumberOfNodesLinF+minimumMissingNodesLinF)
        # print(xyzLinF.shape)

        # set linear fluid points
        pointsLinF  = xyzLinF.transpose().reshape(3*numberOfNodesLinF).copy()

        print("    Number of elements:       "+str(numberOfTetsF))
        # for tetID in range(0, numberOfTetsF, 1):
        #     print("%i %i %i %i" % (tetsLinF[tetID][0], tetsLinF[tetID][1], tetsLinF[tetID][2], tetsLinF[tetID][3]))
        print("    Number of nodes (lin):    "+str(numberOfNodesLinF))
        # for nodeID in range(0, numberOfNodesLinF, 1):
        #     print("%f %f %f" % (xyzLinF[nodeID][0], xyzLinF[nodeID][1], xyzLinF[nodeID][2]))


        # set number of boundary elements
        numberOfBoundaryElementsF   = numberOfTrianglesLinF * 2       # inlet / outlet
        numberOfBoundaryElementsF  += 4 * sc * sl                     # interface
        numberOfBoundaryElementsF  += 2 * (numberOfCircles * sl * 4)  # x0-plane / y0-plane

        # print("        Boundary elements at inlet:      "+str(numberOfTrianglesLinF))
        # print("        Boundary elements at outlet:     "+str(numberOfTrianglesLinF))
        # print("        Boundary elements at interface:  "+str(4 * sc * sl))
        # print("        Boundary elements at x = 0:      "+str((numberOfCircles * sl * 4)))
        # print("        Boundary elements at y = 0:      "+str((numberOfCircles * sl * 4)))

        # compute position of quad-nodes
        # print(lin2quadTol)
        numberOfNodesQuadF, maxNumberOfNodesQuadF, pointsQuadF, boundaryElementsLinF, boundaryElementsQuadF \
            = vesselC.lin2quad(pointsLinF, tetsLinF, tetsQuadF, \
                numberOfNodesLinF, numberOfTetsF, numberOfBoundaryElementsF, \
                z0, lz, ri, patchF_z0, patchF_lz, patchF_ri, patchF_x0, patchF_y0, tol, lin2quadTol*2.0, True)
        # for tetID in range(0, boundaryElementsLinF.shape[0], 1):
        #     print("%i %i %i" % (boundaryElementsLinF[tetID][1], boundaryElementsLinF[tetID][2], boundaryElementsLinF[tetID][3]))

        # print(maxNumberOfNodesQuadF)
        # print(numberOfNodesQuadF)
        # print(numberOfNodesLinF)

        # loop over fluid boundary elements at r = ri to move new and quad nodes to ri
        patchF        = np.zeros((numberOfNodesLinF, 1), dtype=int)
        for i in range(0, numberOfBoundaryElementsF, 1):
            for j in range(1, 4, 1):
                currentNodeNumber = boundaryElementsLinF[i][j] - 1 # need zero-based index
                if (patchF[currentNodeNumber] != patchF_ri):
                    patchF[currentNodeNumber] = boundaryElementsLinF[i][4]
            if not(morphFSboundary):
                continue
            if (boundaryElementsQuadF[i][7] != patchF_ri):
                continue
            # new lin nodes
            for j in range(1, 4, 1):
                currentNodeNumber = boundaryElementsLinF[i][j] - 1 # need zero-based index
                currentNodeX = pointsLinF[currentNodeNumber+numberOfNodesLinF*0]
                currentNodeY = pointsLinF[currentNodeNumber+numberOfNodesLinF*1]
                currentNodeR = np.sqrt(currentNodeX * currentNodeX + currentNodeY * currentNodeY)
                # should be r = ri
                newX = currentNodeX * ri / currentNodeR
                newY = currentNodeY * ri / currentNodeR
                pointsLinF[currentNodeNumber+numberOfNodesLinF*0] = newX
                pointsLinF[currentNodeNumber+numberOfNodesLinF*1] = newY
            # new and quad nodes
            for j in range(1, 7, 1):
                currentNodeNumber = boundaryElementsQuadF[i][j] - 1 # need zero-based index
                currentNodeX = pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*0]
                currentNodeY = pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*1]
                currentNodeR = np.sqrt(currentNodeX * currentNodeX + currentNodeY * currentNodeY)
                # should be r = ri
                newX = currentNodeX * ri / currentNodeR
                newY = currentNodeY * ri / currentNodeR
                pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*0] = newX
                pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*1] = newY
    else:
        print("    Number of nodes (lin):  "+str(numberOfNodesLinF))
        # brute-force method of creating element edge adjacency map
        adjacencyTriF = -np.ones((numberOfTrianglesLinF, numberOfTrianglesLinF), dtype=int)
        edgeI = np.zeros((2), dtype=int)
        edgeJ = np.zeros((2), dtype=int)
        for i in range(0, numberOfTrianglesLinF, 1):
            for j in range(0, i, 1):
                for k in range(0, 3, 1):
                    for l in range(0, 3, 1):
                        # edges with nodes in counter-clockwise order
                        # i.e. for comparisions we need to reverse order once
                        edgeI[0] = tri.simplices[i][k]
                        edgeI[1] = tri.simplices[i][k+1-3*(k==2)]
                        edgeJ[1] = tri.simplices[j][l]
                        edgeJ[0] = tri.simplices[j][l+1-3*(l==2)]
                        if ((edgeI[0] == edgeJ[0]) and (edgeI[1] == edgeJ[1])):
                            # we found a common edge, assign number of edge
                            adjacencyTriF[i][j] = k
                            adjacencyTriF[j][i] = l

        # plot Delaunay triangulation
        # fig = plt.figure()
        # plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
        # plt.plot(points[:,0], points[:,1], 'o')
        # plt.show()
        # return

        # compute graph problem for correct tesselation prism-->tet
        # brute-force version of algorithm to solve graph problem described in YinHanGuYau2011

        # possible tupels corresponding to triangle edges or rather face splitting
        possibleTupels = np.zeros((6, 3), dtype=int)
        possibleTupels[0][0] =  1
        possibleTupels[0][1] =  1
        possibleTupels[0][2] = -1
        possibleTupels[1][0] =  1
        possibleTupels[1][1] = -1
        possibleTupels[1][2] =  1
        possibleTupels[2][0] =  1
        possibleTupels[2][1] = -1
        possibleTupels[2][2] = -1
        possibleTupels[3][0] = -1
        possibleTupels[3][1] =  1
        possibleTupels[3][2] =  1
        possibleTupels[4][0] = -1
        possibleTupels[4][1] =  1
        possibleTupels[4][2] = -1
        possibleTupels[5][0] = -1
        possibleTupels[5][1] = -1
        possibleTupels[5][2] =  1
        # assign a tupel for each of the simplices in the Delaunay triangulation
        tupels = -np.ones((numberOfTrianglesLinF), dtype=int)

        # approximate maximum recursion depth
        worstCaseRecursionDepth = numberOfTrianglesLinF
        if (worstCaseRecursionDepth > sys.getrecursionlimit()):
            print(">>>WARNING: worst case recursion depth is " \
                +str(worstCaseRecursionDepth)+". Changing to " \
                +str(worstCaseRecursionDepth+10)+".")
            sys.setrecursionlimit(worstCaseRecursionDepth+10)
        # solve the graph problem by recursion
        invalid   = False
        alltested = False
        tupels, invalid, alltested = \
            vesselC.assignTupels(tri.simplices, 0, adjacencyTriF, \
            possibleTupels, tupels, invalid, alltested)
        # check whether we found a solution to the graph problem
        if invalid:
            print(">>>ERROR: Invalid tesselation prism-->tet.")
            sys.exit()
        else:
            logging.debug("Tesselation graph problem solved successfully.")

        # make indices 1-based
        disk_trianglesLinF =  tri.simplices+1

        # allocate arrays for prisms and tets
        disk_numberOfTrianglesLinF = disk_trianglesLinF.shape[0]
        numberOfPrismsF = sl * disk_numberOfTrianglesLinF
        numberOfTetsF   = 3 * numberOfPrismsF
        prismsLinF = np.zeros((numberOfPrismsF, 6), dtype=int)

        # compute mesh connectivity for prisms
        for j in range(0, sl, 1):
            for i in range(0, disk_numberOfTrianglesLinF, 1):
                prismsLinF[i+j*disk_numberOfTrianglesLinF][0:3:1] = \
                    disk_trianglesLinF[i][0:3:1] + disk_numberOfNodesLinF * (j + 0)
                prismsLinF[i+j*disk_numberOfTrianglesLinF][3:6:1] = \
                    disk_trianglesLinF[i][0:3:1] + disk_numberOfNodesLinF * (j + 1)

        # prisms --> tets
        # note: Delaunay in 2D constructs the mesh such that the orientation
        #       of the triangles is counter-clockwise
        # note: split corresponds to solution of graph problem and docs/prism-tets.png
        tetsLinF  = np.zeros((numberOfTetsF,  4), dtype=int)
        tetsQuadF = np.zeros((numberOfTetsF, 10), dtype=int)
        tetNumber = 0
        for i in range(0, numberOfPrismsF, 1):
            if (tupels[np.mod(i, numberOfTrianglesLinF)] == 0):
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][1]
                tetsLinF[tetNumber][2] = prismsLinF[i][2]
                tetsLinF[tetNumber][3] = prismsLinF[i][5]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][4]
                tetsLinF[tetNumber][2] = prismsLinF[i][1]
                tetsLinF[tetNumber][3] = prismsLinF[i][5]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][4]
                tetsLinF[tetNumber][2] = prismsLinF[i][5]
                tetsLinF[tetNumber][3] = prismsLinF[i][3]
                tetNumber += 1
            if (tupels[np.mod(i, numberOfTrianglesLinF)] == 1):
                tetsLinF[tetNumber][0] = prismsLinF[i][2]
                tetsLinF[tetNumber][1] = prismsLinF[i][0]
                tetsLinF[tetNumber][2] = prismsLinF[i][1]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][2]
                tetsLinF[tetNumber][1] = prismsLinF[i][3]
                tetsLinF[tetNumber][2] = prismsLinF[i][0]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][2]
                tetsLinF[tetNumber][1] = prismsLinF[i][3]
                tetsLinF[tetNumber][2] = prismsLinF[i][4]
                tetsLinF[tetNumber][3] = prismsLinF[i][5]
                tetNumber += 1
            if (tupels[np.mod(i, numberOfTrianglesLinF)] == 2):
                tetsLinF[tetNumber][0] = prismsLinF[i][1]
                tetsLinF[tetNumber][1] = prismsLinF[i][2]
                tetsLinF[tetNumber][2] = prismsLinF[i][0]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][2]
                tetsLinF[tetNumber][1] = prismsLinF[i][5]
                tetsLinF[tetNumber][2] = prismsLinF[i][0]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][4]
                tetsLinF[tetNumber][2] = prismsLinF[i][5]
                tetsLinF[tetNumber][3] = prismsLinF[i][3]
                tetNumber += 1
            if (tupels[np.mod(i, numberOfTrianglesLinF)] == 3):
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][1]
                tetsLinF[tetNumber][2] = prismsLinF[i][2]
                tetsLinF[tetNumber][3] = prismsLinF[i][3]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][3]
                tetsLinF[tetNumber][1] = prismsLinF[i][1]
                tetsLinF[tetNumber][2] = prismsLinF[i][5]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][1]
                tetsLinF[tetNumber][1] = prismsLinF[i][2]
                tetsLinF[tetNumber][2] = prismsLinF[i][3]
                tetsLinF[tetNumber][3] = prismsLinF[i][5]
                tetNumber += 1
            if (tupels[np.mod(i, numberOfTrianglesLinF)] == 4):
                tetsLinF[tetNumber][0] = prismsLinF[i][2]
                tetsLinF[tetNumber][1] = prismsLinF[i][0]
                tetsLinF[tetNumber][2] = prismsLinF[i][1]
                tetsLinF[tetNumber][3] = prismsLinF[i][5]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][1]
                tetsLinF[tetNumber][2] = prismsLinF[i][5]
                tetsLinF[tetNumber][3] = prismsLinF[i][3]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][1]
                tetsLinF[tetNumber][1] = prismsLinF[i][5]
                tetsLinF[tetNumber][2] = prismsLinF[i][3]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
            if (tupels[np.mod(i, numberOfTrianglesLinF)] == 5):
                tetsLinF[tetNumber][0] = prismsLinF[i][0]
                tetsLinF[tetNumber][1] = prismsLinF[i][1]
                tetsLinF[tetNumber][2] = prismsLinF[i][2]
                tetsLinF[tetNumber][3] = prismsLinF[i][3]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][1]
                tetsLinF[tetNumber][1] = prismsLinF[i][2]
                tetsLinF[tetNumber][2] = prismsLinF[i][3]
                tetsLinF[tetNumber][3] = prismsLinF[i][4]
                tetNumber += 1
                tetsLinF[tetNumber][0] = prismsLinF[i][4]
                tetsLinF[tetNumber][1] = prismsLinF[i][2]
                tetsLinF[tetNumber][2] = prismsLinF[i][3]
                tetsLinF[tetNumber][3] = prismsLinF[i][5]
                tetNumber += 1
        # double-check whether the tesselation is valid
        if (numberOfTetsF != tetNumber):
            print(">>>ERROR: Invalid number of tets!")
            return

        print("    Number of elements:       "+str(numberOfTetsF))

        # set number of boundary elements
        numberOfBoundaryElementsF   = numberOfTrianglesLinF * 2       # inlet / outlet
        numberOfBoundaryElementsF  += 2 * sc * sl                     # interface
        numberOfBoundaryElementsF  += 2 * (numberOfCircles * sl * 2)  # x0-plane / y0-plane

        # compute position of quad-nodes
        numberOfNodesQuadF, maxNumberOfNodesQuadF, pointsQuadF, boundaryElementsLinF, boundaryElementsQuadF \
            = vesselC.lin2quad(pointsLinF, tetsLinF, tetsQuadF, \
                numberOfNodesLinF, numberOfTetsF, numberOfBoundaryElementsF, \
                z0, lz, ri, patchF_z0, patchF_lz, patchF_ri, patchF_x0, patchF_y0, tol, tol, True)

        # loop over fluid boundary elements at r = ri to move quad nodes to ri
        visitedQuadF = np.zeros((numberOfNodesQuadF-numberOfNodesLinF, 1), dtype=int)
        patchF = np.zeros((numberOfNodesLinF, 1), dtype=int)
        for i in range(0, numberOfBoundaryElementsF, 1):
            for j in range(1, 4, 1):
                currentNodeNumber = boundaryElementsLinF[i][j] - 1 # need zero-based index
                if (patchF[currentNodeNumber] != patchF_ri):
                    patchF[currentNodeNumber] = boundaryElementsLinF[i][4]
            if not(morphFSboundary):
                continue
            if (boundaryElementsQuadF[i][7] != patchF_ri):
                continue
            for j in range(4, 7, 1):
                currentNodeNumber = boundaryElementsQuadF[i][j] - 1 # need zero-based index
                if (visitedQuadF[currentNodeNumber-numberOfNodesLinF] == 1):
                    continue
                visitedQuadF[currentNodeNumber-numberOfNodesLinF] = 1
                currentNodeX = pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*0]
                currentNodeY = pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*1]
                currentNodeR = np.sqrt(currentNodeX * currentNodeX + currentNodeY * currentNodeY)
                if (abs(currentNodeR - ri) < tol):
                    # if r = ri
                    continue
                else:
                    # should be r = ri
                    newX = currentNodeX * ri / currentNodeR
                    newY = currentNodeY * ri / currentNodeR
                    pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*0] = newX
                    pointsQuadF[currentNodeNumber+maxNumberOfNodesQuadF*1] = newY

    print("    Number of nodes (quad):   "+str(numberOfNodesQuadF))
    print("!==================================================================!")
    print("Creating tri mesh for LM domain...")

    # get number of elements
    numberOfElementsLM = 0
    for i in range(0, numberOfBoundaryElementsF, 1):
        if (boundaryElementsQuadF[i][7] == patchF_ri):
            numberOfElementsLM += 1

    # get element node IDs
    trisLinLM      = np.zeros((numberOfElementsLM, 3), dtype=int)
    trisQuadLM     = np.zeros((numberOfElementsLM, 6), dtype=int)
    elementsLMiter = 0
    for i in range(0, numberOfBoundaryElementsF, 1):
        if (boundaryElementsQuadF[i][7] == patchF_ri):
            trisQuadLM[elementsLMiter][0:6:1] = boundaryElementsQuadF[i][1:7:1]
            elementsLMiter += 1

    # sanity check
    if (elementsLMiter != numberOfElementsLM):
        print(">>>ERROR: Invalid number of LM elements.")
        return

    # get node coordinates
    present         = -np.ones((maxNumberOfNodesQuadF),    dtype=int)
    pointsQuadLMtmp =  np.zeros((maxNumberOfNodesQuadF*3), dtype=float)
    for i in range(0, numberOfElementsLM, 1):
        for j in range(0, 6, 1):
            present[trisQuadLM[i][j]-1] = 1
            pointsQuadLMtmp[0*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1] = \
                pointsQuadF[0*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1]
            pointsQuadLMtmp[1*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1] = \
                pointsQuadF[1*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1]
            pointsQuadLMtmp[2*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1] = \
                pointsQuadF[2*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1]
            #print(pointsQuadLMtmp[0*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1], \
            #    pointsQuadLMtmp[1*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1], \
            #    pointsQuadLMtmp[2*maxNumberOfNodesQuadF+trisQuadLM[i][j]-1])

    # count number of nodes in LM domain
    numberOfNodesQuadLM = 0
    newID = -np.ones((maxNumberOfNodesQuadF), dtype=int)
    for i in range(0, maxNumberOfNodesQuadF, 1):
        if (present[i] == 1):
            numberOfNodesQuadLM += 1
            newID[i] = numberOfNodesQuadLM # 1-based

    # sanity check
    if solveGraphProblem:
        if (numberOfNodesQuadLM != (2*sc+1)*(2*sl+1)):
            print(">>>ERROR: Invalid number of LM nodes!")
            return
    else:
        checkNumberOfNodesQuadLM = (8 * sc + 2) * sl + sc * 2 + 1
        if (numberOfNodesQuadLM != checkNumberOfNodesQuadLM):
            print(">>>ERROR: Invalid number of LM nodes!")
            return

    # get nodes-quad
    numberOfNodesLinLM  = (sc + 1) * (sl + 1)
    pointsLinLM         = np.zeros((numberOfNodesLinLM,  3), dtype=float)
    pointsQuadLM        = np.zeros((numberOfNodesQuadLM, 3), dtype=float)
    for i in range(0, maxNumberOfNodesQuadF, 1):
        if (present[i] == 1):
            pointsQuadLM[newID[i]-1][0] = \
                pointsQuadLMtmp[0*maxNumberOfNodesQuadF+i]
            pointsQuadLM[newID[i]-1][1] = \
                pointsQuadLMtmp[1*maxNumberOfNodesQuadF+i]
            pointsQuadLM[newID[i]-1][2] = \
                pointsQuadLMtmp[2*maxNumberOfNodesQuadF+i]
    # get nodes-lin
    for i in range(0, numberOfNodesLinLM, 1):
        pointsLinLM[i][0:3:1] = pointsQuadLM[i][0:3:1]

    # adjust node numbers in connectivity array
    for i in range(0, numberOfElementsLM, 1):
        for j in range(0, 6, 1):
            trisQuadLM[i][j] = newID[trisQuadLM[i][j]-1]

    # get connectivity array for linear topology
    for i in range(0, numberOfElementsLM, 1):
        trisLinLM[i][0:3:1] = trisQuadLM[i][0:3:1]

    # sanity checks
    for i in range(0, numberOfElementsLM, 1):
        for j in range(0, 6, 1):
            if (trisQuadLM[i][j] > numberOfNodesQuadLM):
                print(">>>ERROR: Node ID is larger than number of nodes!")
                return
    for i in range(0, numberOfNodesQuadLM, 1):
        for j in range(i+1, numberOfNodesQuadLM, 1):
            if ((abs(pointsQuadLM[i][0] - pointsQuadLM[j][0]) < tol) \
                and (abs(pointsQuadLM[i][1] - pointsQuadLM[j][1]) < tol) \
                and (abs(pointsQuadLM[i][2] - pointsQuadLM[j][2]) < tol)):
                    print(">>>ERROR: Node positions are non-unique!")
                    return

    # get boundary patches
    numberOfBoundaryElementsLinLM   = 2 * sc + 2 * sl
    numberOfBoundaryElementsQuadLM  = 2 * sc + 2 * sl
    boundaryElementsLinLM           = np.zeros((numberOfBoundaryElementsLinLM,  4), dtype=int)
    boundaryElementsQuadLM          = np.zeros((numberOfBoundaryElementsQuadLM, 5), dtype=int)
    patchLinLM                      = np.zeros((numberOfNodesLinLM),                dtype=int)
    patchQuadLM                     = np.zeros((numberOfNodesQuadLM),               dtype=int)
    boundaryElementsQuadLMiter      = 0
    for i in range(0, numberOfElementsLM, 1):
        n0x = pointsQuadLM[trisQuadLM[i][0]-1][0]
        n0y = pointsQuadLM[trisQuadLM[i][0]-1][1]
        n0z = pointsQuadLM[trisQuadLM[i][0]-1][2]
        n1x = pointsQuadLM[trisQuadLM[i][1]-1][0]
        n1y = pointsQuadLM[trisQuadLM[i][1]-1][1]
        n1z = pointsQuadLM[trisQuadLM[i][1]-1][2]
        n2x = pointsQuadLM[trisQuadLM[i][2]-1][0]
        n2y = pointsQuadLM[trisQuadLM[i][2]-1][1]
        n2z = pointsQuadLM[trisQuadLM[i][2]-1][2]
        # z = 0
        if ((abs(n0z) < tol) and (abs(n1z) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_z0
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_z0
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_z0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][3]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_z0
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_z0
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_z0
            patchQuadLM[trisQuadLM[i][3]-1] = patchLM_z0
            boundaryElementsQuadLMiter += 1
        if ((abs(n0z) < tol) and (abs(n2z) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_z0
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_z0
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_z0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][4]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_z0
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_z0
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_z0
            patchQuadLM[trisQuadLM[i][4]-1] = patchLM_z0
            boundaryElementsQuadLMiter += 1
        if ((abs(n1z) < tol) and (abs(n2z) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_z0
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_z0
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_z0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][5]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_z0
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_z0
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_z0
            patchQuadLM[trisQuadLM[i][5]-1] = patchLM_z0
            boundaryElementsQuadLMiter += 1
        # z = lz
        if ((abs(n0z-lz) < tol) and (abs(n1z-lz) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_lz
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_lz
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_lz
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][3]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_lz
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_lz
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_lz
            patchQuadLM[trisQuadLM[i][3]-1] = patchLM_lz
            boundaryElementsQuadLMiter += 1
        if ((abs(n0z-lz) < tol) and (abs(n2z-lz) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_lz
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_lz
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_lz
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][4]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_lz
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_lz
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_lz
            patchQuadLM[trisQuadLM[i][4]-1] = patchLM_lz
            boundaryElementsQuadLMiter += 1
        if ((abs(n1z-lz) < tol) and (abs(n2z-lz) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_lz
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_lz
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_lz
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][5]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_lz
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_lz
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_lz
            patchQuadLM[trisQuadLM[i][5]-1] = patchLM_lz
            boundaryElementsQuadLMiter += 1
        # x = 0
        if ((abs(n0x) < tol) and (abs(n1x) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_x0
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_x0
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_x0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][3]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_x0
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_x0
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_x0
            patchQuadLM[trisQuadLM[i][3]-1] = patchLM_x0
            boundaryElementsQuadLMiter += 1
        if ((abs(n0x) < tol) and (abs(n2x) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_x0
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_x0
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_x0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][4]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_x0
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_x0
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_x0
            patchQuadLM[trisQuadLM[i][4]-1] = patchLM_x0
            boundaryElementsQuadLMiter += 1
        if ((abs(n1x) < tol) and (abs(n2x) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_x0
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_x0
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_x0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][5]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_x0
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_x0
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_x0
            patchQuadLM[trisQuadLM[i][5]-1] = patchLM_x0
            boundaryElementsQuadLMiter += 1
        # y = 0
        if ((abs(n0y) < tol) and (abs(n1y) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_y0
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_y0
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_y0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][3]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_y0
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_y0
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_y0
            patchQuadLM[trisQuadLM[i][3]-1] = patchLM_y0
            boundaryElementsQuadLMiter += 1
        if ((abs(n0y) < tol) and (abs(n2y) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_y0
            patchLinLM[trisQuadLM[i][0]-1] = patchLM_y0
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_y0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][0]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][4]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_y0
            patchQuadLM[trisQuadLM[i][0]-1] = patchLM_y0
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_y0
            patchQuadLM[trisQuadLM[i][4]-1] = patchLM_y0
            boundaryElementsQuadLMiter += 1
        if ((abs(n1y) < tol) and (abs(n2y) < tol)):
            boundaryElementsLinLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsLinLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsLinLM[boundaryElementsQuadLMiter][3] = patchLM_y0
            patchLinLM[trisQuadLM[i][1]-1] = patchLM_y0
            patchLinLM[trisQuadLM[i][2]-1] = patchLM_y0
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][0] = i + 1
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][1] = trisQuadLM[i][1]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][2] = trisQuadLM[i][2]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][3] = trisQuadLM[i][5]
            boundaryElementsQuadLM[boundaryElementsQuadLMiter][4] = patchLM_y0
            patchQuadLM[trisQuadLM[i][1]-1] = patchLM_y0
            patchQuadLM[trisQuadLM[i][2]-1] = patchLM_y0
            patchQuadLM[trisQuadLM[i][5]-1] = patchLM_y0
            boundaryElementsQuadLMiter += 1

    # sanity check
    if (boundaryElementsQuadLMiter != numberOfBoundaryElementsQuadLM):
        print(">>>ERROR: Invalid number of LM boundary elements."+str(boundaryElementsQuadLMiter)+" "+str(numberOfBoundaryElementsQuadLM))
        return

    # move all points if origin (x0, y0, z0) is not (0, 0, 0)
    if (abs(x0) > tol):
        pointsLinF[0*numberOfNodesLinF:1*numberOfNodesLinF:1]           += x0
        pointsQuadF[0*maxNumberOfNodesQuadF:1*maxNumberOfNodesQuadF:1]  += x0
        pointsQuadLM[0:numberOfNodesQuadLM:1][0]                        += x0
    if (abs(y0) > tol):
        pointsLinF[1*numberOfNodesLinF:2*numberOfNodesLinF:1]           += y0
        pointsQuadF[1*maxNumberOfNodesQuadF:2*maxNumberOfNodesQuadF:1]  += y0
        pointsQuadLM[0:numberOfNodesQuadLM:1][1]                        += y0
    if (abs(z0) > tol):
        pointsLinF[2*numberOfNodesLinF:3*numberOfNodesLinF:1]           += z0
        pointsQuadF[2*maxNumberOfNodesQuadF:3*maxNumberOfNodesQuadF:1]  += z0
        pointsQuadLM[0:numberOfNodesQuadLM:1][2]                        += z0

    print("    Number of nodes (quad): "+str(numberOfNodesQuadLM))
    print("    Number of elements:     "+str(numberOfElementsLM))
    print("!==================================================================!")
    print("Export:")

    # export patch definition as SolidPres, FluidPres and LMult for sanity check
    fname = patchDir+"/SolidPres-1.D"
    myheader = str(numberOfNodesLinS) + " 1"
    np.savetxt(fname, \
        patchS, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")
    fname = patchDir+"/FluidPres-1.D"
    myheader = str(numberOfNodesLinF) + " 1"
    np.savetxt(fname, \
        patchF, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")
    fname = patchDir+"/LMult-1.D"
    myheader = str(numberOfNodesQuadLM) + " 1"
    np.savetxt(fname, \
        patchQuadLM, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    # export X/T/B files
    fname = fileprefixS + "_lin_FE.X"
    print("    "+fname)
    myheader = str(numberOfNodesLinS) + " 3"
    np.savetxt(fname, \
        pointsLinS, \
        fmt="%.18e", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixS + "_quad_FE.X"
    print("    "+fname)
    myheader = str(numberOfNodesQuadS) + " 3"
    np.savetxt(fname, \
        pointsQuadS, \
        fmt="%.18e", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixS + "_lin_FE.T"
    print("    "+fname)
    myheader = str(numberOfElementsS) + " " + str(numberOfNodesLinS)
    np.savetxt(fname, \
        np.delete(elementNodeListS, range(8,27,1), axis=1), \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixS + "_quad_FE.T"
    print("    "+fname)
    myheader = str(numberOfElementsS) + " " + str(numberOfNodesQuadS)
    np.savetxt(fname, \
        elementNodeListS, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixS + "_lin_FE.B"
    print("    "+fname)
    myheader = str(numberOfBoundaryElementsS)
    np.savetxt(fname, \
        boundaryPatchesLinS, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixS + "_quad_FE.B"
    print("    "+fname)
    myheader = str(numberOfBoundaryElementsS)
    np.savetxt(fname, \
        boundaryPatchesQuadS, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    # export X/T/B files
    fname = fileprefixF + "_lin_FE.X"
    print("    "+fname)
    myheader = str(int(numberOfNodesLinF)) + " 3"
    np.savetxt(fname, \
        pointsLinF.reshape((3, numberOfNodesLinF)).transpose(), \
        fmt="%.18e", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixF + "_quad_FE.X"
    print("    "+fname)
    myheader = str(int(numberOfNodesQuadF)) + " 3"
    exportPointsQuadF = np.delete( \
        pointsQuadF.reshape((3, maxNumberOfNodesQuadF)).transpose(), \
        range(numberOfNodesQuadF, maxNumberOfNodesQuadF, 1), \
        axis=0)
    np.savetxt(fname, \
        exportPointsQuadF, \
        fmt="%.18e", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixF + "_lin_FE.T"
    print("    "+fname)
    myheader = str(int(numberOfTetsF)) + " " + str(int(numberOfNodesLinF))
    np.savetxt(fname, \
        tetsLinF, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixF + "_quad_FE.T"
    print("    "+fname)
    myheader = str(int(numberOfTetsF)) + " " + str(int(numberOfNodesQuadF))
    np.savetxt(fname, \
        tetsQuadF, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixF + "_lin_FE.B"
    print("    "+fname)
    myheader = str(int(numberOfBoundaryElementsF))
    np.savetxt(fname, \
        boundaryElementsLinF, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixF + "_quad_FE.B"
    print("    "+fname)
    myheader = str(int(numberOfBoundaryElementsF))
    np.savetxt(fname, \
        boundaryElementsQuadF, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    # export X/T/B files
    fname = fileprefixLM + "_lin_FE.X"
    print("    "+fname)
    myheader = str(int(numberOfNodesLinLM)) + " 3"
    np.savetxt(fname, \
        pointsLinLM, \
        fmt="%18e", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixLM + "_lin_FE.T"
    print("    "+fname)
    myheader = str(int(numberOfElementsLM)) + " " + str(int(numberOfNodesLinLM))
    np.savetxt(fname, \
        trisLinLM, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixLM + "_lin_FE.B"
    print("    "+fname)
    myheader = str(int(numberOfBoundaryElementsLinLM))
    np.savetxt(fname, \
        boundaryElementsLinLM, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixLM + "_quad_FE.X"
    print("    "+fname)
    myheader = str(int(numberOfNodesQuadLM)) + " 3"
    np.savetxt(fname, \
        pointsQuadLM, \
        fmt="%18e", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixLM + "_quad_FE.T"
    print("    "+fname)
    myheader = str(int(numberOfElementsLM)) + " " + str(int(numberOfNodesQuadLM))
    np.savetxt(fname, \
        trisQuadLM, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

    fname = fileprefixLM + "_quad_FE.B"
    print("    "+fname)
    myheader = str(int(numberOfBoundaryElementsQuadLM))
    np.savetxt(fname, \
        boundaryElementsQuadLM, \
        fmt="%i", delimiter=" ", newline="\n", \
        header=myheader, footer="", comments="")

#    # plot fluid mesh nodes
#    x = pointsLinF[0*numberOfNodesLinF:1*numberOfNodesLinF:1]
#    y = pointsLinF[1*numberOfNodesLinF:2*numberOfNodesLinF:1]
#    z = pointsLinF[2*numberOfNodesLinF:3*numberOfNodesLinF:1]
#    x = pointsQuadF[0*maxNumberOfNodesQuadF:0*maxNumberOfNodesQuadF+numberOfNodesQuadF:1]
#    y = pointsQuadF[1*maxNumberOfNodesQuadF:1*maxNumberOfNodesQuadF+numberOfNodesQuadF:1]
#    z = pointsQuadF[2*maxNumberOfNodesQuadF:2*maxNumberOfNodesQuadF+numberOfNodesQuadF:1]
#    fig = plt.figure()
#    ax = Axes3D(fig)
#    axscatter = ax.scatter(x, y, z)#c=scalarMap.to_rgba(mycolors))
##    scalarMap.set_array(mycolors)
##    fig.colorbar(scalarMap)
#    plt.show()

    print("!==================================================================!")


if __name__ == "__main__":
    main()
